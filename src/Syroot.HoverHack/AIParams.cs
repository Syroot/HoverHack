﻿using System.IO;

namespace Syroot.HoverHack
{
    /// <summary>
    /// Represents opponent car behavior parameters.
    /// </summary>
    internal class AIParams
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Initializes a new instance of the <see cref="AIParams"/> class from the given byte array.
        /// </summary>
        /// <param name="rawData">The raw byte array data.</param>
        internal AIParams(byte[] rawData)
        {
            using (MemoryStream stream = new MemoryStream(rawData))
            using (BinaryReader reader = new BinaryReader(stream))
            {
                BackupTurnRate = reader.ReadInt32() / 65536f;
                BackupGameCycles = reader.ReadInt32();
                BackupSpeedFactor = reader.ReadInt32() / 65536f;
                MaxTargetAngleAdjust = reader.ReadInt32();
                MaxAngleForAcceleration = reader.ReadInt32();
                MaxDistToAcquireHuman = reader.ReadInt32();
                MaxDistToAcquireFlag = reader.ReadInt32();
                MaxBeaconDistance = reader.ReadInt32();
                MaxBeaconHeightDifference = reader.ReadInt32();
                BeaconSearchDepth = reader.ReadInt32();
            }
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the chance of turning in backwards driving a drone will take when being hit and confused.
        /// </summary>
        internal float BackupTurnRate { get; set; }

        /// <summary>
        /// Gets or sets the length of backwards driving a drone will take when being hit and confused.
        /// </summary>
        internal int BackupGameCycles { get; set; }

        /// <summary>
        /// Gets or sets the speed of backwards driving a drone will take when being hit and confused, relative to the
        /// normal driving speed.
        /// </summary>
        internal float BackupSpeedFactor { get; set; }

        /// <summary>
        /// Gets or sets the maximum angle a drone will turn to start targeting (unsure).
        /// </summary>
        internal int MaxTargetAngleAdjust { get; set; }

        /// <summary>
        /// Gets or sets the angle a target has to be in before the drone accelerates towards it.
        /// </summary>
        internal int MaxAngleForAcceleration { get; set; }

        /// <summary>
        /// Gets or sets the maximum distance before a green drone sees the human car and targets it.
        /// </summary>
        internal int MaxDistToAcquireHuman { get; set; }

        /// <summary>
        /// Gets or sets the maximum distance before a blue drone sees a red flag and targets it.
        /// </summary>
        internal int MaxDistToAcquireFlag { get; set; }

        /// <summary>
        /// Gets or sets the maximum distance to a waypoint before a drone queries it as the next target.
        /// </summary>
        internal int MaxBeaconDistance { get; set; }

        /// <summary>
        /// Gets or sets the height at which a drone can see a waypoint and will query it as the next target.
        /// </summary>
        internal int MaxBeaconHeightDifference { get; set; }

        /// <summary>
        /// Gets or sets a value that probably represents the number of beacons to validate before taking their route,
        /// making sure no other drone is using them now.
        /// </summary>
        internal int BeaconSearchDepth { get; set; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Serializes the parameters back into a raw byte array.
        /// </summary>
        /// <returns>The serialized byte array.</returns>
        internal byte[] GetRawData()
        {
            byte[] buffer = new byte[40];

            using (MemoryStream stream = new MemoryStream(buffer, true))
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write((int)(BackupTurnRate * 65536f));
                writer.Write(BackupGameCycles);
                writer.Write((int)(BackupSpeedFactor * 65536f));
                writer.Write(MaxTargetAngleAdjust);
                writer.Write(MaxAngleForAcceleration);
                writer.Write(MaxDistToAcquireHuman);
                writer.Write(MaxDistToAcquireFlag);
                writer.Write(MaxBeaconDistance);
                writer.Write(MaxBeaconHeightDifference);
                writer.Write(BeaconSearchDepth);
            }

            return buffer;
        }
    }
}
