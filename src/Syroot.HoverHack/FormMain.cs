﻿using System;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Syroot.HoverHack
{
    /// <summary>
    /// The main window of the application.
    /// </summary>
    public partial class FormMain : Form
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        #region ---- Default Values ----

        private static byte[] _humanBeginnerDefault = new byte[]
        {
            0x00, 0x00, 0x01, 0x00, 0x34, 0xF3, 0xFF, 0xFF, 0x00, 0x00, 0x32, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80,
            0x00, 0x00, 0x33, 0x33, 0x00, 0x00, 0x33, 0x33, 0x00, 0x00, 0xF5, 0xA8, 0x00, 0x00, 0x7A, 0x54, 0x01, 0x00,
            0x99, 0x19, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00
        };
        private static byte[] _humanIntermediateDefault = new byte[]
        {
            0x00, 0x00, 0x03, 0x00, 0x71, 0xFD, 0xFF, 0xFF, 0x00, 0x00, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80,
            0x00, 0x00, 0xCC, 0x4C, 0x00, 0x00, 0xCC, 0x4C, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x7A, 0x54, 0x01, 0x00,
            0x66, 0x06, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00
        };
        private static byte[] _humanExpertDefault = new byte[]
        {
            0x00, 0x00, 0x05, 0x00, 0xBF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x4B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80,
            0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x66, 0x66, 0x00, 0x00, 0x66, 0x66, 0x00, 0x00, 0x7A, 0x54, 0x01, 0x00,
            0x8F, 0x02, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x00
        };

        private static byte[] _droneBeginnerDefault = new byte[]
        {
            0x00, 0x00, 0x01, 0x00, 0x71, 0xFD, 0xFF, 0xFF, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x14, 0x00, 0xCC, 0xCC,
            0x00, 0x00, 0xCC, 0xCC, 0x00, 0x00, 0xCC, 0x4C, 0x00, 0x00, 0xF5, 0xA8, 0x00, 0x00, 0x7A, 0x54, 0x01, 0x00,
            0x66, 0x06, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00
        };
        private static byte[] _droneIntermediateDefault = new byte[]
        {
            0x00, 0x00, 0x01, 0x00, 0x71, 0xFD, 0xFF, 0xFF, 0x00, 0x00, 0x32, 0x00, 0x00, 0x00, 0x14, 0x00, 0xCC, 0xCC,
            0x00, 0x00, 0xCC, 0xCC, 0x00, 0x00, 0xCC, 0x4C, 0x00, 0x00, 0xF5, 0xA8, 0x00, 0x00, 0x7A, 0x54, 0x01, 0x00,
            0x66, 0x06, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00
        };
        private static byte[] _droneExpertDefault = new byte[]
        {
            0x00, 0x00, 0x01, 0x00, 0x71, 0xFD, 0xFF, 0xFF, 0x00, 0x00, 0x3C, 0x00, 0x00, 0x00, 0x14, 0x00, 0xCC, 0xCC,
            0x00, 0x00, 0xCC, 0xCC, 0x00, 0x00, 0xCC, 0x4C, 0x00, 0x00, 0xF5, 0xA8, 0x00, 0x00, 0x7A, 0x54, 0x01, 0x00,
            0x66, 0x06, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00
        };

        private static byte[] _aiBeginnerDefault = new byte[]
        {
            0x00, 0x80, 0x01, 0x00, 0x3C, 0x00, 0x00, 0x00, 0x66, 0x66, 0x00, 0x00, 0x1E, 0x00, 0x00, 0x00, 0x0F, 0x00,
            0x00, 0x00, 0x88, 0x13, 0x00, 0x00, 0x88, 0x13, 0x00, 0x00, 0x88, 0x13, 0x00, 0x00, 0x64, 0x00, 0x00, 0x00,
            0x03, 0x00, 0x00, 0x00
        };
        private static byte[] _aiIntermediateDefault = new byte[]
        {
            0x00, 0x80, 0x01, 0x00, 0x3C, 0x00, 0x00, 0x00, 0x66, 0x66, 0x00, 0x00, 0x1E, 0x00, 0x00, 0x00, 0x0F, 0x00,
            0x00, 0x00, 0x58, 0x1B, 0x00, 0x00, 0x88, 0x13, 0x00, 0x00, 0x88, 0x13, 0x00, 0x00, 0x64, 0x00, 0x00, 0x00,
            0x04, 0x00, 0x00, 0x00
        };
        private static byte[] _aiExpertDefault = new byte[]
        {
            0x00, 0x80, 0x01, 0x00, 0x3C, 0x00, 0x00, 0x00, 0x66, 0x66, 0x00, 0x00, 0x1E, 0x00, 0x00, 0x00, 0x0F, 0x00,
            0x00, 0x00, 0x10, 0x27, 0x00, 0x00, 0x88, 0x13, 0x00, 0x00, 0x88, 0x13, 0x00, 0x00, 0x64, 0x00, 0x00, 0x00,
            0x04, 0x00, 0x00, 0x00
        };

        #endregion

        private RegistryKey _hoverKey;

        private CarParams _humanBeginner;
        private CarParams _humanIntermediate;
        private CarParams _humanExpert;
        private CarParams _droneBeginner;
        private CarParams _droneIntermediate;
        private CarParams _droneExpert;
        private AIParams _aiBeginner;
        private AIParams _aiIntermediate;
        private AIParams _aiExpert;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FormMain"/> class.
        /// </summary>
        public FormMain()
        {
            InitializeComponent();

            // Open the Hover key in the registry and load its settings. It is getting created if it does not exist yet.
            string keyPath = Path.Combine(Path.Combine("SOFTWARE", "Microsoft"), "Hover!");
            Registry.CurrentUser.CreateSubKey(keyPath);
            _hoverKey = Registry.CurrentUser.OpenSubKey(keyPath, true);
            LoadRegistry();

            // Write the values to the user interface and attach user events.
            UpdateUIFromData();
            AttachTextBoxEvents(this);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void LoadRegistry()
        {
            _humanBeginner = new CarParams((byte[])_hoverKey.GetValue("HBegParams", _humanBeginnerDefault));
            _humanIntermediate = new CarParams((byte[])_hoverKey.GetValue("HIntParams", _humanIntermediateDefault));
            _humanExpert = new CarParams((byte[])_hoverKey.GetValue("HExpParams", _humanExpertDefault));

            _droneBeginner = new CarParams((byte[])_hoverKey.GetValue("RBegParams", _droneBeginnerDefault));
            _droneIntermediate = new CarParams((byte[])_hoverKey.GetValue("RIntParams", _droneIntermediateDefault));
            _droneExpert = new CarParams((byte[])_hoverKey.GetValue("RExpParams", _droneExpertDefault));

            _aiBeginner = new AIParams((byte[])_hoverKey.GetValue("RobotAIBegParams", _aiBeginnerDefault));
            _aiIntermediate = new AIParams((byte[])_hoverKey.GetValue("RobotAIIntParams", _aiIntermediateDefault));
            _aiExpert = new AIParams((byte[])_hoverKey.GetValue("RobotAIExpParams", _aiExpertDefault));
        }

        private void SaveRegistry()
        {
            _hoverKey.SetValue("HBegParams", _humanBeginner.GetRawData(), RegistryValueKind.Binary);
            _hoverKey.SetValue("HIntParams", _humanIntermediate.GetRawData(), RegistryValueKind.Binary);
            _hoverKey.SetValue("HExpParams", _humanExpert.GetRawData(), RegistryValueKind.Binary);

            _hoverKey.SetValue("RBegParams", _droneBeginner.GetRawData(), RegistryValueKind.Binary);
            _hoverKey.SetValue("RIntParams", _droneIntermediate.GetRawData(), RegistryValueKind.Binary);
            _hoverKey.SetValue("RExpParams", _droneExpert.GetRawData(), RegistryValueKind.Binary);

            _hoverKey.SetValue("RobotAIBegParams", _aiBeginner.GetRawData(), RegistryValueKind.Binary);
            _hoverKey.SetValue("RobotAIIntParams", _aiIntermediate.GetRawData(), RegistryValueKind.Binary);
            _hoverKey.SetValue("RobotAIExpParams", _aiExpert.GetRawData(), RegistryValueKind.Binary);
        }

        private void ClearRegistry()
        {
            _hoverKey.DeleteValue("HBegParams", false);
            _hoverKey.DeleteValue("HIntParams", false);
            _hoverKey.DeleteValue("HExpParams", false);

            _hoverKey.DeleteValue("RBegParams", false);
            _hoverKey.DeleteValue("RIntParams", false);
            _hoverKey.DeleteValue("RExpParams", false);

            _hoverKey.DeleteValue("RobotAIBegParams", false);
            _hoverKey.DeleteValue("RobotAIIntParams", false);
            _hoverKey.DeleteValue("RobotAIExpParams", false);
        }

        private void AttachTextBoxEvents(Control parent)
        {
            foreach (Control control in parent.Controls)
            {
                IntegerTextBox integerTextBox = control as IntegerTextBox;
                if (integerTextBox != null)
                {
                    integerTextBox.ValueChanged += NumericTextBox_ValueChanged;
                    continue;
                }

                FloatTextBox floatTextBox = control as FloatTextBox;
                if (floatTextBox != null)
                {
                    floatTextBox.ValueChanged += NumericTextBox_ValueChanged;
                    continue;
                }

                AttachTextBoxEvents(control);
            }
        }

        private void UpdateUIFromData()
        {
            // Human Beginner
            _tbPlayerBeginnerMaxVelocity.Value = _humanBeginner.MaxVelocity;
            _tbPlayerBeginnerNormalAccel.Value = _humanBeginner.NormalAccel;
            _tbPlayerBeginnerTurnRate.Value = _humanBeginner.TurnRate;
            _tbPlayerBeginnerFrictionConstant.Value = _humanBeginner.FrictionConstant;
            _tbPlayerBeginnerTurnLagFactor.Value = _humanBeginner.TurnLagFactor;
            _tbPlayerBeginnerRotationStability.Value = _humanBeginner.RotationStability;
            _tbPlayerBeginnerRotationKick.Value = _humanBeginner.RotationKick;
            _tbPlayerBeginnerRotationAccel.Value = _humanBeginner.RotationAccel;
            _tbPlayerBeginnerSpeedMultiplier.Value = _humanBeginner.SpeedMultiplier;
            _tbPlayerBeginnerSlowMultiplier.Value = _humanBeginner.SlowMultiplier;
            _tbPlayerBeginnerAcquireVelocity.Value = _humanBeginner.AcquireVelocity;

            // Human Intermediate
            _tbPlayerIntermediateMaxVelocity.Value = _humanIntermediate.MaxVelocity;
            _tbPlayerIntermediateNormalAccel.Value = _humanIntermediate.NormalAccel;
            _tbPlayerIntermediateTurnRate.Value = _humanIntermediate.TurnRate;
            _tbPlayerIntermediateFrictionConstant.Value = _humanIntermediate.FrictionConstant;
            _tbPlayerIntermediateTurnLagFactor.Value = _humanIntermediate.TurnLagFactor;
            _tbPlayerIntermediateRotationStability.Value = _humanIntermediate.RotationStability;
            _tbPlayerIntermediateRotationKick.Value = _humanIntermediate.RotationKick;
            _tbPlayerIntermediateRotationAccel.Value = _humanIntermediate.RotationAccel;
            _tbPlayerIntermediateSpeedMultiplier.Value = _humanIntermediate.SpeedMultiplier;
            _tbPlayerIntermediateSlowMultiplier.Value = _humanIntermediate.SlowMultiplier;
            _tbPlayerIntermediateAcquireVelocity.Value = _humanIntermediate.AcquireVelocity;

            // Human Expert
            _tbPlayerExpertMaxVelocity.Value = _humanExpert.MaxVelocity;
            _tbPlayerExpertNormalAccel.Value = _humanExpert.NormalAccel;
            _tbPlayerExpertTurnRate.Value = _humanExpert.TurnRate;
            _tbPlayerExpertFrictionConstant.Value = _humanExpert.FrictionConstant;
            _tbPlayerExpertTurnLagFactor.Value = _humanExpert.TurnLagFactor;
            _tbPlayerExpertRotationStability.Value = _humanExpert.RotationStability;
            _tbPlayerExpertRotationKick.Value = _humanExpert.RotationKick;
            _tbPlayerExpertRotationAccel.Value = _humanExpert.RotationAccel;
            _tbPlayerExpertSpeedMultiplier.Value = _humanExpert.SpeedMultiplier;
            _tbPlayerExpertSlowMultiplier.Value = _humanExpert.SlowMultiplier;
            _tbPlayerExpertAcquireVelocity.Value = _humanExpert.AcquireVelocity;

            // Drone Beginner
            _tbDroneBeginnerMaxVelocity.Value = _droneBeginner.MaxVelocity;
            _tbDroneBeginnerAquireVelocity.Value = _droneBeginner.AcquireVelocity;
            _tbDroneBeginnerNormalAccel.Value = _droneBeginner.NormalAccel;
            _tbDroneBeginnerTurnRate.Value = _droneBeginner.TurnRate;
            _tbDroneBeginnerFrictionConstant.Value = _droneBeginner.FrictionConstant;
            _tbDroneBeginnerTurnLagFactor.Value = _droneBeginner.TurnLagFactor;
            _tbDroneBeginnerRotationStability.Value = _droneBeginner.RotationStability;
            _tbDroneBeginnerRotationKick.Value = _droneBeginner.RotationKick;
            _tbDroneBeginnerRotationAccel.Value = _droneBeginner.RotationAccel;
            _tbDroneBeginnerSpeedMultiplier.Value = _droneBeginner.SpeedMultiplier;
            _tbDroneBeginnerSlowMultiplier.Value = _droneBeginner.SlowMultiplier;

            // Drone Intermediate
            _tbDroneIntermediateMaxVelocity.Value = _droneIntermediate.MaxVelocity;
            _tbDroneIntermediateAquireVelocity.Value = _droneIntermediate.AcquireVelocity;
            _tbDroneIntermediateNormalAccel.Value = _droneIntermediate.NormalAccel;
            _tbDroneIntermediateTurnRate.Value = _droneIntermediate.TurnRate;
            _tbDroneIntermediateFrictionConstant.Value = _droneIntermediate.FrictionConstant;
            _tbDroneIntermediateTurnLagFactor.Value = _droneIntermediate.TurnLagFactor;
            _tbDroneIntermediateRotationStability.Value = _droneIntermediate.RotationStability;
            _tbDroneIntermediateRotationKick.Value = _droneIntermediate.RotationKick;
            _tbDroneIntermediateRotationAccel.Value = _droneIntermediate.RotationAccel;
            _tbDroneIntermediateSpeedMultiplier.Value = _droneIntermediate.SpeedMultiplier;
            _tbDroneIntermediateSlowMultiplier.Value = _droneIntermediate.SlowMultiplier;

            // Drone Expert
            _tbDroneExpertMaxVelocity.Value = _droneExpert.MaxVelocity;
            _tbDroneExpertAquireVelocity.Value = _droneExpert.AcquireVelocity;
            _tbDroneExpertNormalAccel.Value = _droneExpert.NormalAccel;
            _tbDroneExpertTurnRate.Value = _droneExpert.TurnRate;
            _tbDroneExpertFrictionConstant.Value = _droneExpert.FrictionConstant;
            _tbDroneExpertTurnLagFactor.Value = _droneExpert.TurnLagFactor;
            _tbDroneExpertRotationStability.Value = _droneExpert.RotationStability;
            _tbDroneExpertRotationKick.Value = _droneExpert.RotationKick;
            _tbDroneExpertRotationAccel.Value = _droneExpert.RotationAccel;
            _tbDroneExpertSpeedMultiplier.Value = _droneExpert.SpeedMultiplier;
            _tbDroneExpertSlowMultiplier.Value = _droneExpert.SlowMultiplier;

            // AI Beginner
            _tbAIBeginnerBackupTurnRate.Value = _aiBeginner.BackupTurnRate;
            _tbAIBeginnerBackupGameCycles.Value = _aiBeginner.BackupGameCycles;
            _tbAIBeginnerBackupSpeedFactor.Value = _aiBeginner.BackupSpeedFactor;
            _tbAIBeginnerMaxTargetAngleAdjust.Value = _aiBeginner.MaxTargetAngleAdjust;
            _tbAIBeginnerMaxAngleForAcceleration.Value = _aiBeginner.MaxAngleForAcceleration;
            _tbAIBeginnerMaxDistToAcquireHuman.Value = _aiBeginner.MaxDistToAcquireHuman;
            _tbAIBeginnerMaxDistToAcquireFlag.Value = _aiBeginner.MaxDistToAcquireFlag;
            _tbAIBeginnerMaxBeaconDistance.Value = _aiBeginner.MaxBeaconDistance;
            _tbAIBeginnerMaxBeaconHeightDifference.Value = _aiBeginner.MaxBeaconHeightDifference;
            _tbAIBeginnerBeaconSearchDepth.Value = _aiBeginner.BeaconSearchDepth;

            // AI Intermediate
            _tbAIIntermediateBackupTurnRate.Value = _aiIntermediate.BackupTurnRate;
            _tbAIIntermediateBackupGameCycles.Value = _aiIntermediate.BackupGameCycles;
            _tbAIIntermediateBackupSpeedFactor.Value = _aiIntermediate.BackupSpeedFactor;
            _tbAIIntermediateMaxTargetAngleAdjust.Value = _aiIntermediate.MaxTargetAngleAdjust;
            _tbAIIntermediateMaxAngleForAcceleration.Value = _aiIntermediate.MaxAngleForAcceleration;
            _tbAIIntermediateMaxDistToAcquireHuman.Value = _aiIntermediate.MaxDistToAcquireHuman;
            _tbAIIntermediateMaxDistToAcquireFlag.Value = _aiIntermediate.MaxDistToAcquireFlag;
            _tbAIIntermediateMaxBeaconDistance.Value = _aiIntermediate.MaxBeaconDistance;
            _tbAIIntermediateMaxBeaconHeightDifference.Value = _aiIntermediate.MaxBeaconHeightDifference;
            _tbAIIntermediateBeaconSearchDepth.Value = _aiIntermediate.BeaconSearchDepth;

            // AI Expert
            _tbAIExpertBackupTurnRate.Value = _aiExpert.BackupTurnRate;
            _tbAIExpertBackupGameCycles.Value = _aiExpert.BackupGameCycles;
            _tbAIExpertBackupSpeedFactor.Value = _aiExpert.BackupSpeedFactor;
            _tbAIExpertMaxTargetAngleAdjust.Value = _aiExpert.MaxTargetAngleAdjust;
            _tbAIExpertMaxAngleForAcceleration.Value = _aiExpert.MaxAngleForAcceleration;
            _tbAIExpertMaxDistToAcquireHuman.Value = _aiExpert.MaxDistToAcquireHuman;
            _tbAIExpertMaxDistToAcquireFlag.Value = _aiExpert.MaxDistToAcquireFlag;
            _tbAIExpertMaxBeaconDistance.Value = _aiExpert.MaxBeaconDistance;
            _tbAIExpertMaxBeaconHeightDifference.Value = _aiExpert.MaxBeaconHeightDifference;
            _tbAIExpertBeaconSearchDepth.Value = _aiExpert.BeaconSearchDepth;
        }

        private void UpdateDataFromUI()
        {
            // Human Beginner
            _humanBeginner.MaxVelocity = _tbPlayerBeginnerMaxVelocity.Value;
            _humanBeginner.NormalAccel = _tbPlayerBeginnerNormalAccel.Value;
            _humanBeginner.TurnRate = _tbPlayerBeginnerTurnRate.Value;
            _humanBeginner.FrictionConstant = _tbPlayerBeginnerFrictionConstant.Value;
            _humanBeginner.TurnLagFactor = _tbPlayerBeginnerTurnLagFactor.Value;
            _humanBeginner.RotationStability = _tbPlayerBeginnerRotationStability.Value;
            _humanBeginner.RotationKick = _tbPlayerBeginnerRotationKick.Value;
            _humanBeginner.RotationAccel = _tbPlayerBeginnerRotationAccel.Value;
            _humanBeginner.SpeedMultiplier = _tbPlayerBeginnerSpeedMultiplier.Value;
            _humanBeginner.SlowMultiplier = _tbPlayerBeginnerSlowMultiplier.Value;
            _humanBeginner.AcquireVelocity = _tbPlayerBeginnerAcquireVelocity.Value;

            // Human Intermediate
            _humanIntermediate.MaxVelocity = _tbPlayerIntermediateMaxVelocity.Value;
            _humanIntermediate.NormalAccel = _tbPlayerIntermediateNormalAccel.Value;
            _humanIntermediate.TurnRate = _tbPlayerIntermediateTurnRate.Value;
            _humanIntermediate.FrictionConstant = _tbPlayerIntermediateFrictionConstant.Value;
            _humanIntermediate.TurnLagFactor = _tbPlayerIntermediateTurnLagFactor.Value;
            _humanIntermediate.RotationStability = _tbPlayerIntermediateRotationStability.Value;
            _humanIntermediate.RotationKick = _tbPlayerIntermediateRotationKick.Value;
            _humanIntermediate.RotationAccel = _tbPlayerIntermediateRotationAccel.Value;
            _humanIntermediate.SpeedMultiplier = _tbPlayerIntermediateSpeedMultiplier.Value;
            _humanIntermediate.SlowMultiplier = _tbPlayerIntermediateSlowMultiplier.Value;
            _humanIntermediate.AcquireVelocity = _tbPlayerIntermediateAcquireVelocity.Value;

            // Human Expert
            _humanExpert.MaxVelocity = _tbPlayerExpertMaxVelocity.Value;
            _humanExpert.NormalAccel = _tbPlayerExpertNormalAccel.Value;
            _humanExpert.TurnRate = _tbPlayerExpertTurnRate.Value;
            _humanExpert.FrictionConstant = _tbPlayerExpertFrictionConstant.Value;
            _humanExpert.TurnLagFactor = _tbPlayerExpertTurnLagFactor.Value;
            _humanExpert.RotationStability = _tbPlayerExpertRotationStability.Value;
            _humanExpert.RotationKick = _tbPlayerExpertRotationKick.Value;
            _humanExpert.RotationAccel = _tbPlayerExpertRotationAccel.Value;
            _humanExpert.SpeedMultiplier = _tbPlayerExpertSpeedMultiplier.Value;
            _humanExpert.SlowMultiplier = _tbPlayerExpertSlowMultiplier.Value;
            _humanExpert.AcquireVelocity = _tbPlayerExpertAcquireVelocity.Value;

            // Drone Beginner
            _droneBeginner.MaxVelocity = _tbDroneBeginnerMaxVelocity.Value;
            _droneBeginner.AcquireVelocity = _tbDroneBeginnerAquireVelocity.Value;
            _droneBeginner.NormalAccel = _tbDroneBeginnerNormalAccel.Value;
            _droneBeginner.TurnRate = _tbDroneBeginnerTurnRate.Value;
            _droneBeginner.FrictionConstant = _tbDroneBeginnerFrictionConstant.Value;
            _droneBeginner.TurnLagFactor = _tbDroneBeginnerTurnLagFactor.Value;
            _droneBeginner.RotationStability = _tbDroneBeginnerRotationStability.Value;
            _droneBeginner.RotationKick = _tbDroneBeginnerRotationKick.Value;
            _droneBeginner.RotationAccel = _tbDroneBeginnerRotationAccel.Value;
            _droneBeginner.SpeedMultiplier = _tbDroneBeginnerSpeedMultiplier.Value;
            _droneBeginner.SlowMultiplier = _tbDroneBeginnerSlowMultiplier.Value;

            // Drone Intermediate
            _droneIntermediate.MaxVelocity = _tbDroneIntermediateMaxVelocity.Value;
            _droneIntermediate.AcquireVelocity = _tbDroneIntermediateAquireVelocity.Value;
            _droneIntermediate.NormalAccel = _tbDroneIntermediateNormalAccel.Value;
            _droneIntermediate.TurnRate = _tbDroneIntermediateTurnRate.Value;
            _droneIntermediate.FrictionConstant = _tbDroneIntermediateFrictionConstant.Value;
            _droneIntermediate.TurnLagFactor = _tbDroneIntermediateTurnLagFactor.Value;
            _droneIntermediate.RotationStability = _tbDroneIntermediateRotationStability.Value;
            _droneIntermediate.RotationKick = _tbDroneIntermediateRotationKick.Value;
            _droneIntermediate.RotationAccel = _tbDroneIntermediateRotationAccel.Value;
            _droneIntermediate.SpeedMultiplier = _tbDroneIntermediateSpeedMultiplier.Value;
            _droneIntermediate.SlowMultiplier = _tbDroneIntermediateSlowMultiplier.Value;

            // Drone Expert
            _droneExpert.MaxVelocity = _tbDroneExpertMaxVelocity.Value;
            _droneExpert.AcquireVelocity = _tbDroneExpertAquireVelocity.Value;
            _droneExpert.NormalAccel = _tbDroneExpertNormalAccel.Value;
            _droneExpert.TurnRate = _tbDroneExpertTurnRate.Value;
            _droneExpert.FrictionConstant = _tbDroneExpertFrictionConstant.Value;
            _droneExpert.TurnLagFactor = _tbDroneExpertTurnLagFactor.Value;
            _droneExpert.RotationStability = _tbDroneExpertRotationStability.Value;
            _droneExpert.RotationKick = _tbDroneExpertRotationKick.Value;
            _droneExpert.RotationAccel = _tbDroneExpertRotationAccel.Value;
            _droneExpert.SpeedMultiplier = _tbDroneExpertSpeedMultiplier.Value;
            _droneExpert.SlowMultiplier = _tbDroneExpertSlowMultiplier.Value;

            // AI Beginner
            _aiBeginner.BackupTurnRate = _tbAIBeginnerBackupTurnRate.Value;
            _aiBeginner.BackupGameCycles = _tbAIBeginnerBackupGameCycles.Value;
            _aiBeginner.BackupSpeedFactor = _tbAIBeginnerBackupSpeedFactor.Value;
            _aiBeginner.MaxTargetAngleAdjust = _tbAIBeginnerMaxTargetAngleAdjust.Value;
            _aiBeginner.MaxAngleForAcceleration = _tbAIBeginnerMaxAngleForAcceleration.Value;
            _aiBeginner.MaxDistToAcquireHuman = _tbAIBeginnerMaxDistToAcquireHuman.Value;
            _aiBeginner.MaxDistToAcquireFlag = _tbAIBeginnerMaxDistToAcquireFlag.Value;
            _aiBeginner.MaxBeaconDistance = _tbAIBeginnerMaxBeaconDistance.Value;
            _aiBeginner.MaxBeaconHeightDifference = _tbAIBeginnerMaxBeaconHeightDifference.Value;
            _aiBeginner.BeaconSearchDepth = _tbAIBeginnerBeaconSearchDepth.Value;

            // AI Intermediate
            _aiIntermediate.BackupTurnRate = _tbAIIntermediateBackupTurnRate.Value;
            _aiIntermediate.BackupGameCycles = _tbAIIntermediateBackupGameCycles.Value;
            _aiIntermediate.BackupSpeedFactor = _tbAIIntermediateBackupSpeedFactor.Value;
            _aiIntermediate.MaxTargetAngleAdjust = _tbAIIntermediateMaxTargetAngleAdjust.Value;
            _aiIntermediate.MaxAngleForAcceleration = _tbAIIntermediateMaxAngleForAcceleration.Value;
            _aiIntermediate.MaxDistToAcquireHuman = _tbAIIntermediateMaxDistToAcquireHuman.Value;
            _aiIntermediate.MaxDistToAcquireFlag = _tbAIIntermediateMaxDistToAcquireFlag.Value;
            _aiIntermediate.MaxBeaconDistance = _tbAIIntermediateMaxBeaconDistance.Value;
            _aiIntermediate.MaxBeaconHeightDifference = _tbAIIntermediateMaxBeaconHeightDifference.Value;
            _aiIntermediate.BeaconSearchDepth = _tbAIIntermediateBeaconSearchDepth.Value;

            // AI Expert
            _aiExpert.BackupTurnRate = _tbAIExpertBackupTurnRate.Value;
            _aiExpert.BackupGameCycles = _tbAIExpertBackupGameCycles.Value;
            _aiExpert.BackupSpeedFactor = _tbAIExpertBackupSpeedFactor.Value;
            _aiExpert.MaxTargetAngleAdjust = _tbAIExpertMaxTargetAngleAdjust.Value;
            _aiExpert.MaxAngleForAcceleration = _tbAIExpertMaxAngleForAcceleration.Value;
            _aiExpert.MaxDistToAcquireHuman = _tbAIExpertMaxDistToAcquireHuman.Value;
            _aiExpert.MaxDistToAcquireFlag = _tbAIExpertMaxDistToAcquireFlag.Value;
            _aiExpert.MaxBeaconDistance = _tbAIExpertMaxBeaconDistance.Value;
            _aiExpert.MaxBeaconHeightDifference = _tbAIExpertMaxBeaconHeightDifference.Value;
            _aiExpert.BeaconSearchDepth = _tbAIExpertBeaconSearchDepth.Value;
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void NumericTextBox_ValueChanged(object sender, EventArgs e)
        {
            UpdateDataFromUI();
        }

        private void _btSaveRegistry_Click(object sender, EventArgs e)
        {
            SaveRegistry();

            // Reloading the saved settings can't hurt, in case something went wrong, it becomes visible.
            LoadRegistry();
            UpdateUIFromData();
        }

        private void _btReloadRegistry_Click(object sender, EventArgs e)
        {
            LoadRegistry();
            UpdateUIFromData();
        }

        private void _btClearRegistry_Click(object sender, EventArgs e)
        {
            ClearRegistry();

            // Since the keys have been deleted, LoadRegistry will load default values and display them.
            LoadRegistry();
            UpdateUIFromData();
        }
    }
}
