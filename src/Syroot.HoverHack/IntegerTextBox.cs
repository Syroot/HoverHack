﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Syroot.HoverHack
{
    /// <summary>
    /// Represents a <see cref="NumericTextBox"/> only accepting integer numbers.
    /// </summary>
    public class IntegerTextBox : NumericTextBox
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="IntegerTextBox"/> class.
        /// </summary>
        public IntegerTextBox()
        {
            BackColor = Color.LightBlue;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets or sets the represented numerical value.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Value
        {
            get
            {
                return (int)TryGetValue();
            }
            set
            {
                Text = value.ToString();
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        /// <summary>
        /// Tries to return the numerical value of the text in the <see cref="NumericTextBox"/>.
        /// </summary>
        /// <returns>The numerical value of the text in the <see cref="NumericTextBox"/>.</returns>
        protected override object TryGetValue()
        {
            int value;
            if (Int32.TryParse(Text, out value))
            {
                return value;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Tries to set the given text as the new numerical value of the <see cref="NumericTextBox"/>.
        /// </summary>
        /// <param name="text">The text to convert into the numerical representation.</param>
        /// <returns>The text to eventually display in the <see cref="NumericTextBox"/>.</returns>
        protected override string TrySetValue(string text)
        {
            int value;
            if (Int32.TryParse(text, out value))
            {
                return value.ToString();
            }
            else
            {
                return "0";
            }
        }
    }
}
