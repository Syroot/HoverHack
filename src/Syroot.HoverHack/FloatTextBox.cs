﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Syroot.HoverHack
{
    /// <summary>
    /// Represents a <see cref="NumericTextBox"/> only accepting float numbers.
    /// </summary>
    public class FloatTextBox : NumericTextBox
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FloatTextBox"/> class.
        /// </summary>
        public FloatTextBox()
        {
            BackColor = Color.LightGreen;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets or sets the represented numerical value.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float Value
        {
            get
            {
                return (float)TryGetValue();
            }
            set
            {
                Text = value.ToString();
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        /// <summary>
        /// Tries to return the numerical value of the text in the <see cref="NumericTextBox"/>.
        /// </summary>
        /// <returns>The numerical value of the text in the <see cref="NumericTextBox"/>.</returns>
        protected override object TryGetValue()
        {
            float value;
            if (Single.TryParse(Text, out value))
            {
                return value;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Tries to set the given text as the new numerical value of the <see cref="NumericTextBox"/>.
        /// </summary>
        /// <param name="text">The text to convert into the numerical representation.</param>
        /// <returns>The text to eventually display in the <see cref="NumericTextBox"/>.</returns>
        protected override string TrySetValue(string text)
        {
            float value;
            if (Single.TryParse(text, out value))
            {
                return value.ToString("0.####");
            }
            else
            {
                return "0";
            }
        }
    }
}
