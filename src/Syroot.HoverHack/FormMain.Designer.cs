﻿namespace Syroot.HoverHack
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this._tcSettings = new System.Windows.Forms.TabControl();
            this._tpPlayer = new System.Windows.Forms.TabPage();
            this._gbPlayerSpeedMultiplier = new System.Windows.Forms.GroupBox();
            this._tbPlayerExpertSlowMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerIntermediateSlowMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerBeginnerSlowMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerExpertSpeedMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerIntermediateSpeedMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerBeginnerSpeedMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._lbPlayerSlowMultiplier = new System.Windows.Forms.Label();
            this._lbPlayerSpeedMultiplier = new System.Windows.Forms.Label();
            this._lbPlayerRotationAccel = new System.Windows.Forms.Label();
            this._lbPlayerRotationKick = new System.Windows.Forms.Label();
            this._lbPlayerAcquireVelocity = new System.Windows.Forms.Label();
            this._lbPlayerRotationStability = new System.Windows.Forms.Label();
            this._lbPlayerTurnLagFactor = new System.Windows.Forms.Label();
            this._lbPlayerFrictionConstant = new System.Windows.Forms.Label();
            this._lbPlayerTurnRate = new System.Windows.Forms.Label();
            this._lbPlayerNormalAccel = new System.Windows.Forms.Label();
            this._lbPlayerMaxVelocity = new System.Windows.Forms.Label();
            this._gbPlayerExpert = new System.Windows.Forms.GroupBox();
            this._tbPlayerExpertRotationAccel = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerExpertRotationKick = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerExpertAcquireVelocity = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerExpertRotationStability = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerExpertTurnLagFactor = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerExpertFrictionConstant = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerExpertTurnRate = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerExpertNormalAccel = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerExpertMaxVelocity = new Syroot.HoverHack.FloatTextBox();
            this._gbPlayerIntermediate = new System.Windows.Forms.GroupBox();
            this._tbPlayerIntermediateRotationAccel = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerIntermediateRotationKick = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerIntermediateRotationStability = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerIntermediateAcquireVelocity = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerIntermediateTurnLagFactor = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerIntermediateFrictionConstant = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerIntermediateTurnRate = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerIntermediateNormalAccel = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerIntermediateMaxVelocity = new Syroot.HoverHack.FloatTextBox();
            this._gbPlayerBeginner = new System.Windows.Forms.GroupBox();
            this._tbPlayerBeginnerRotationAccel = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerBeginnerRotationKick = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerBeginnerRotationStability = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerBeginnerTurnLagFactor = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerBeginnerAcquireVelocity = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerBeginnerFrictionConstant = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerBeginnerTurnRate = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerBeginnerNormalAccel = new Syroot.HoverHack.FloatTextBox();
            this._tbPlayerBeginnerMaxVelocity = new Syroot.HoverHack.FloatTextBox();
            this._tpDrone = new System.Windows.Forms.TabPage();
            this._gbDroneSpeedMultiplier = new System.Windows.Forms.GroupBox();
            this._lbDroneSpeedMultiplier = new System.Windows.Forms.Label();
            this._tbDroneExpertSlowMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneBeginnerSpeedMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneIntermediateSlowMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneIntermediateSpeedMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneBeginnerSlowMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneExpertSpeedMultiplier = new Syroot.HoverHack.FloatTextBox();
            this._lbDroneSlowMultiplier = new System.Windows.Forms.Label();
            this._lbDroneRotationAccel = new System.Windows.Forms.Label();
            this._lbDroneRotationKick = new System.Windows.Forms.Label();
            this._lbDroneRotationStability = new System.Windows.Forms.Label();
            this._lbDroneTurnLagFactor = new System.Windows.Forms.Label();
            this._lbDroneFrictionConstant = new System.Windows.Forms.Label();
            this._lbDroneTurnRate = new System.Windows.Forms.Label();
            this._lbDroneNormalAccel = new System.Windows.Forms.Label();
            this._lbDroneAcquireVelocity = new System.Windows.Forms.Label();
            this._lbDroneMaxVelocity = new System.Windows.Forms.Label();
            this._gbDroneExpert = new System.Windows.Forms.GroupBox();
            this._tbDroneExpertRotationAccel = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneExpertMaxVelocity = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneExpertFrictionConstant = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneExpertRotationKick = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneExpertTurnRate = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneExpertAquireVelocity = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneExpertTurnLagFactor = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneExpertRotationStability = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneExpertNormalAccel = new Syroot.HoverHack.FloatTextBox();
            this._gbDroneIntermediate = new System.Windows.Forms.GroupBox();
            this._tbDroneIntermediateRotationAccel = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneIntermediateMaxVelocity = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneIntermediateRotationKick = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneIntermediateAquireVelocity = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneIntermediateRotationStability = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneIntermediateNormalAccel = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneIntermediateTurnLagFactor = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneIntermediateTurnRate = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneIntermediateFrictionConstant = new Syroot.HoverHack.FloatTextBox();
            this._gbDroneBeginner = new System.Windows.Forms.GroupBox();
            this._tbDroneBeginnerRotationAccel = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneBeginnerRotationKick = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneBeginnerRotationStability = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneBeginnerTurnLagFactor = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneBeginnerFrictionConstant = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneBeginnerTurnRate = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneBeginnerNormalAccel = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneBeginnerAquireVelocity = new Syroot.HoverHack.FloatTextBox();
            this._tbDroneBeginnerMaxVelocity = new Syroot.HoverHack.FloatTextBox();
            this._tpAI = new System.Windows.Forms.TabPage();
            this._lbAIBeaconSearchDepth = new System.Windows.Forms.Label();
            this._lbAIMaxBeaconHeightDifference = new System.Windows.Forms.Label();
            this._lbAIMaxBeaconDistance = new System.Windows.Forms.Label();
            this._lbAIMaxDistanceToAcquireFlag = new System.Windows.Forms.Label();
            this._lbAIMaxDistToAcquireHuman = new System.Windows.Forms.Label();
            this._lbAIMaxAngleForAcceleration = new System.Windows.Forms.Label();
            this._lbAIMaxTargetAngleAdjust = new System.Windows.Forms.Label();
            this._lbAIBackupSpeedFactor = new System.Windows.Forms.Label();
            this._lbAIBackupGameCycles = new System.Windows.Forms.Label();
            this._lbAIBackupTurnRate = new System.Windows.Forms.Label();
            this._gbAIExpert = new System.Windows.Forms.GroupBox();
            this._tbAIExpertBeaconSearchDepth = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIExpertMaxBeaconHeightDifference = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIExpertBackupTurnRate = new Syroot.HoverHack.FloatTextBox();
            this._tbAIExpertMaxAngleForAcceleration = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIExpertMaxBeaconDistance = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIExpertMaxTargetAngleAdjust = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIExpertBackupGameCycles = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIExpertMaxDistToAcquireHuman = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIExpertMaxDistToAcquireFlag = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIExpertBackupSpeedFactor = new Syroot.HoverHack.FloatTextBox();
            this._gbAIIntermediate = new System.Windows.Forms.GroupBox();
            this._tbAIIntermediateBeaconSearchDepth = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIIntermediateMaxBeaconHeightDifference = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIIntermediateBackupTurnRate = new Syroot.HoverHack.FloatTextBox();
            this._tbAIIntermediateMaxBeaconDistance = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIIntermediateBackupGameCycles = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIIntermediateMaxDistToAcquireFlag = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIIntermediateBackupSpeedFactor = new Syroot.HoverHack.FloatTextBox();
            this._tbAIIntermediateMaxDistToAcquireHuman = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIIntermediateMaxTargetAngleAdjust = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIIntermediateMaxAngleForAcceleration = new Syroot.HoverHack.IntegerTextBox();
            this._gbAIBeginner = new System.Windows.Forms.GroupBox();
            this._tbAIBeginnerBeaconSearchDepth = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIBeginnerMaxBeaconHeightDifference = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIBeginnerMaxBeaconDistance = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIBeginnerMaxDistToAcquireFlag = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIBeginnerMaxDistToAcquireHuman = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIBeginnerMaxAngleForAcceleration = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIBeginnerMaxTargetAngleAdjust = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIBeginnerBackupSpeedFactor = new Syroot.HoverHack.FloatTextBox();
            this._tbAIBeginnerBackupGameCycles = new Syroot.HoverHack.IntegerTextBox();
            this._tbAIBeginnerBackupTurnRate = new Syroot.HoverHack.FloatTextBox();
            this._btSaveRegistry = new System.Windows.Forms.Button();
            this._btClearRegistry = new System.Windows.Forms.Button();
            this._btReloadRegistry = new System.Windows.Forms.Button();
            this._tcSettings.SuspendLayout();
            this._tpPlayer.SuspendLayout();
            this._gbPlayerSpeedMultiplier.SuspendLayout();
            this._gbPlayerExpert.SuspendLayout();
            this._gbPlayerIntermediate.SuspendLayout();
            this._gbPlayerBeginner.SuspendLayout();
            this._tpDrone.SuspendLayout();
            this._gbDroneSpeedMultiplier.SuspendLayout();
            this._gbDroneExpert.SuspendLayout();
            this._gbDroneIntermediate.SuspendLayout();
            this._gbDroneBeginner.SuspendLayout();
            this._tpAI.SuspendLayout();
            this._gbAIExpert.SuspendLayout();
            this._gbAIIntermediate.SuspendLayout();
            this._gbAIBeginner.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tcSettings
            // 
            this._tcSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tcSettings.Controls.Add(this._tpPlayer);
            this._tcSettings.Controls.Add(this._tpDrone);
            this._tcSettings.Controls.Add(this._tpAI);
            this._tcSettings.Location = new System.Drawing.Point(12, 12);
            this._tcSettings.Name = "_tcSettings";
            this._tcSettings.SelectedIndex = 0;
            this._tcSettings.Size = new System.Drawing.Size(469, 409);
            this._tcSettings.TabIndex = 0;
            // 
            // _tpPlayer
            // 
            this._tpPlayer.Controls.Add(this._gbPlayerSpeedMultiplier);
            this._tpPlayer.Controls.Add(this._lbPlayerRotationAccel);
            this._tpPlayer.Controls.Add(this._lbPlayerRotationKick);
            this._tpPlayer.Controls.Add(this._lbPlayerAcquireVelocity);
            this._tpPlayer.Controls.Add(this._lbPlayerRotationStability);
            this._tpPlayer.Controls.Add(this._lbPlayerTurnLagFactor);
            this._tpPlayer.Controls.Add(this._lbPlayerFrictionConstant);
            this._tpPlayer.Controls.Add(this._lbPlayerTurnRate);
            this._tpPlayer.Controls.Add(this._lbPlayerNormalAccel);
            this._tpPlayer.Controls.Add(this._lbPlayerMaxVelocity);
            this._tpPlayer.Controls.Add(this._gbPlayerExpert);
            this._tpPlayer.Controls.Add(this._gbPlayerIntermediate);
            this._tpPlayer.Controls.Add(this._gbPlayerBeginner);
            this._tpPlayer.Location = new System.Drawing.Point(4, 24);
            this._tpPlayer.Name = "_tpPlayer";
            this._tpPlayer.Padding = new System.Windows.Forms.Padding(3);
            this._tpPlayer.Size = new System.Drawing.Size(461, 381);
            this._tpPlayer.TabIndex = 0;
            this._tpPlayer.Text = "Player Motion Physics";
            this._tpPlayer.UseVisualStyleBackColor = true;
            // 
            // _gbPlayerSpeedMultiplier
            // 
            this._gbPlayerSpeedMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._gbPlayerSpeedMultiplier.Controls.Add(this._tbPlayerExpertSlowMultiplier);
            this._gbPlayerSpeedMultiplier.Controls.Add(this._tbPlayerIntermediateSlowMultiplier);
            this._gbPlayerSpeedMultiplier.Controls.Add(this._tbPlayerBeginnerSlowMultiplier);
            this._gbPlayerSpeedMultiplier.Controls.Add(this._tbPlayerExpertSpeedMultiplier);
            this._gbPlayerSpeedMultiplier.Controls.Add(this._tbPlayerIntermediateSpeedMultiplier);
            this._gbPlayerSpeedMultiplier.Controls.Add(this._tbPlayerBeginnerSpeedMultiplier);
            this._gbPlayerSpeedMultiplier.Controls.Add(this._lbPlayerSlowMultiplier);
            this._gbPlayerSpeedMultiplier.Controls.Add(this._lbPlayerSpeedMultiplier);
            this._gbPlayerSpeedMultiplier.Location = new System.Drawing.Point(34, 295);
            this._gbPlayerSpeedMultiplier.Name = "_gbPlayerSpeedMultiplier";
            this._gbPlayerSpeedMultiplier.Size = new System.Drawing.Size(421, 80);
            this._gbPlayerSpeedMultiplier.TabIndex = 12;
            this._gbPlayerSpeedMultiplier.TabStop = false;
            this._gbPlayerSpeedMultiplier.Text = "Max Velocity Multipliers for Speed and Slow Pods";
            // 
            // _tbPlayerExpertSlowMultiplier
            // 
            this._tbPlayerExpertSlowMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerExpertSlowMultiplier.Location = new System.Drawing.Point(339, 51);
            this._tbPlayerExpertSlowMultiplier.Name = "_tbPlayerExpertSlowMultiplier";
            this._tbPlayerExpertSlowMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerExpertSlowMultiplier.TabIndex = 7;
            // 
            // _tbPlayerIntermediateSlowMultiplier
            // 
            this._tbPlayerIntermediateSlowMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerIntermediateSlowMultiplier.Location = new System.Drawing.Point(245, 51);
            this._tbPlayerIntermediateSlowMultiplier.Name = "_tbPlayerIntermediateSlowMultiplier";
            this._tbPlayerIntermediateSlowMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerIntermediateSlowMultiplier.TabIndex = 5;
            // 
            // _tbPlayerBeginnerSlowMultiplier
            // 
            this._tbPlayerBeginnerSlowMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerBeginnerSlowMultiplier.Location = new System.Drawing.Point(151, 51);
            this._tbPlayerBeginnerSlowMultiplier.Name = "_tbPlayerBeginnerSlowMultiplier";
            this._tbPlayerBeginnerSlowMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerBeginnerSlowMultiplier.TabIndex = 3;
            // 
            // _tbPlayerExpertSpeedMultiplier
            // 
            this._tbPlayerExpertSpeedMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerExpertSpeedMultiplier.Location = new System.Drawing.Point(339, 22);
            this._tbPlayerExpertSpeedMultiplier.Name = "_tbPlayerExpertSpeedMultiplier";
            this._tbPlayerExpertSpeedMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerExpertSpeedMultiplier.TabIndex = 6;
            // 
            // _tbPlayerIntermediateSpeedMultiplier
            // 
            this._tbPlayerIntermediateSpeedMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerIntermediateSpeedMultiplier.Location = new System.Drawing.Point(245, 22);
            this._tbPlayerIntermediateSpeedMultiplier.Name = "_tbPlayerIntermediateSpeedMultiplier";
            this._tbPlayerIntermediateSpeedMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerIntermediateSpeedMultiplier.TabIndex = 4;
            // 
            // _tbPlayerBeginnerSpeedMultiplier
            // 
            this._tbPlayerBeginnerSpeedMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerBeginnerSpeedMultiplier.Location = new System.Drawing.Point(151, 22);
            this._tbPlayerBeginnerSpeedMultiplier.Name = "_tbPlayerBeginnerSpeedMultiplier";
            this._tbPlayerBeginnerSpeedMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerBeginnerSpeedMultiplier.TabIndex = 2;
            // 
            // _lbPlayerSlowMultiplier
            // 
            this._lbPlayerSlowMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbPlayerSlowMultiplier.AutoSize = true;
            this._lbPlayerSlowMultiplier.Location = new System.Drawing.Point(56, 54);
            this._lbPlayerSlowMultiplier.Name = "_lbPlayerSlowMultiplier";
            this._lbPlayerSlowMultiplier.Size = new System.Drawing.Size(89, 15);
            this._lbPlayerSlowMultiplier.TabIndex = 1;
            this._lbPlayerSlowMultiplier.Text = "Slow Multiplier:";
            // 
            // _lbPlayerSpeedMultiplier
            // 
            this._lbPlayerSpeedMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbPlayerSpeedMultiplier.AutoSize = true;
            this._lbPlayerSpeedMultiplier.Location = new System.Drawing.Point(49, 25);
            this._lbPlayerSpeedMultiplier.Name = "_lbPlayerSpeedMultiplier";
            this._lbPlayerSpeedMultiplier.Size = new System.Drawing.Size(96, 15);
            this._lbPlayerSpeedMultiplier.TabIndex = 0;
            this._lbPlayerSpeedMultiplier.Text = "Speed Multiplier:";
            // 
            // _lbPlayerRotationAccel
            // 
            this._lbPlayerRotationAccel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbPlayerRotationAccel.AutoSize = true;
            this._lbPlayerRotationAccel.Location = new System.Drawing.Point(89, 263);
            this._lbPlayerRotationAccel.Name = "_lbPlayerRotationAccel";
            this._lbPlayerRotationAccel.Size = new System.Drawing.Size(84, 15);
            this._lbPlayerRotationAccel.TabIndex = 8;
            this._lbPlayerRotationAccel.Text = "Rotation Accel";
            // 
            // _lbPlayerRotationKick
            // 
            this._lbPlayerRotationKick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbPlayerRotationKick.AutoSize = true;
            this._lbPlayerRotationKick.Location = new System.Drawing.Point(96, 234);
            this._lbPlayerRotationKick.Name = "_lbPlayerRotationKick";
            this._lbPlayerRotationKick.Size = new System.Drawing.Size(77, 15);
            this._lbPlayerRotationKick.TabIndex = 7;
            this._lbPlayerRotationKick.Text = "Rotation Kick";
            // 
            // _lbPlayerAcquireVelocity
            // 
            this._lbPlayerAcquireVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbPlayerAcquireVelocity.AutoSize = true;
            this._lbPlayerAcquireVelocity.Location = new System.Drawing.Point(31, 60);
            this._lbPlayerAcquireVelocity.Name = "_lbPlayerAcquireVelocity";
            this._lbPlayerAcquireVelocity.Size = new System.Drawing.Size(142, 15);
            this._lbPlayerAcquireVelocity.TabIndex = 1;
            this._lbPlayerAcquireVelocity.Text = "Acquire Velocity (unused)";
            // 
            // _lbPlayerRotationStability
            // 
            this._lbPlayerRotationStability.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbPlayerRotationStability.AutoSize = true;
            this._lbPlayerRotationStability.Location = new System.Drawing.Point(76, 205);
            this._lbPlayerRotationStability.Name = "_lbPlayerRotationStability";
            this._lbPlayerRotationStability.Size = new System.Drawing.Size(97, 15);
            this._lbPlayerRotationStability.TabIndex = 6;
            this._lbPlayerRotationStability.Text = "Rotation Stability";
            // 
            // _lbPlayerTurnLagFactor
            // 
            this._lbPlayerTurnLagFactor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbPlayerTurnLagFactor.AutoSize = true;
            this._lbPlayerTurnLagFactor.Location = new System.Drawing.Point(83, 176);
            this._lbPlayerTurnLagFactor.Name = "_lbPlayerTurnLagFactor";
            this._lbPlayerTurnLagFactor.Size = new System.Drawing.Size(90, 15);
            this._lbPlayerTurnLagFactor.TabIndex = 5;
            this._lbPlayerTurnLagFactor.Text = "Turn Lag Factor";
            // 
            // _lbPlayerFrictionConstant
            // 
            this._lbPlayerFrictionConstant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbPlayerFrictionConstant.AutoSize = true;
            this._lbPlayerFrictionConstant.Location = new System.Drawing.Point(75, 147);
            this._lbPlayerFrictionConstant.Name = "_lbPlayerFrictionConstant";
            this._lbPlayerFrictionConstant.Size = new System.Drawing.Size(98, 15);
            this._lbPlayerFrictionConstant.TabIndex = 4;
            this._lbPlayerFrictionConstant.Text = "Friction Constant";
            // 
            // _lbPlayerTurnRate
            // 
            this._lbPlayerTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbPlayerTurnRate.AutoSize = true;
            this._lbPlayerTurnRate.Location = new System.Drawing.Point(115, 118);
            this._lbPlayerTurnRate.Name = "_lbPlayerTurnRate";
            this._lbPlayerTurnRate.Size = new System.Drawing.Size(58, 15);
            this._lbPlayerTurnRate.TabIndex = 3;
            this._lbPlayerTurnRate.Text = "Turn Rate";
            // 
            // _lbPlayerNormalAccel
            // 
            this._lbPlayerNormalAccel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbPlayerNormalAccel.AutoSize = true;
            this._lbPlayerNormalAccel.Location = new System.Drawing.Point(94, 89);
            this._lbPlayerNormalAccel.Name = "_lbPlayerNormalAccel";
            this._lbPlayerNormalAccel.Size = new System.Drawing.Size(79, 15);
            this._lbPlayerNormalAccel.TabIndex = 2;
            this._lbPlayerNormalAccel.Text = "Normal Accel";
            // 
            // _lbPlayerMaxVelocity
            // 
            this._lbPlayerMaxVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbPlayerMaxVelocity.AutoSize = true;
            this._lbPlayerMaxVelocity.Location = new System.Drawing.Point(100, 31);
            this._lbPlayerMaxVelocity.Name = "_lbPlayerMaxVelocity";
            this._lbPlayerMaxVelocity.Size = new System.Drawing.Size(73, 15);
            this._lbPlayerMaxVelocity.TabIndex = 0;
            this._lbPlayerMaxVelocity.Text = "Max Velocity";
            // 
            // _gbPlayerExpert
            // 
            this._gbPlayerExpert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._gbPlayerExpert.Controls.Add(this._tbPlayerExpertRotationAccel);
            this._gbPlayerExpert.Controls.Add(this._tbPlayerExpertRotationKick);
            this._gbPlayerExpert.Controls.Add(this._tbPlayerExpertAcquireVelocity);
            this._gbPlayerExpert.Controls.Add(this._tbPlayerExpertRotationStability);
            this._gbPlayerExpert.Controls.Add(this._tbPlayerExpertTurnLagFactor);
            this._gbPlayerExpert.Controls.Add(this._tbPlayerExpertFrictionConstant);
            this._gbPlayerExpert.Controls.Add(this._tbPlayerExpertTurnRate);
            this._gbPlayerExpert.Controls.Add(this._tbPlayerExpertNormalAccel);
            this._gbPlayerExpert.Controls.Add(this._tbPlayerExpertMaxVelocity);
            this._gbPlayerExpert.Location = new System.Drawing.Point(367, 6);
            this._gbPlayerExpert.Name = "_gbPlayerExpert";
            this._gbPlayerExpert.Size = new System.Drawing.Size(88, 283);
            this._gbPlayerExpert.TabIndex = 11;
            this._gbPlayerExpert.TabStop = false;
            this._gbPlayerExpert.Text = "Expert";
            // 
            // _tbPlayerExpertRotationAccel
            // 
            this._tbPlayerExpertRotationAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerExpertRotationAccel.Location = new System.Drawing.Point(6, 254);
            this._tbPlayerExpertRotationAccel.Name = "_tbPlayerExpertRotationAccel";
            this._tbPlayerExpertRotationAccel.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerExpertRotationAccel.TabIndex = 8;
            // 
            // _tbPlayerExpertRotationKick
            // 
            this._tbPlayerExpertRotationKick.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerExpertRotationKick.Location = new System.Drawing.Point(6, 225);
            this._tbPlayerExpertRotationKick.Name = "_tbPlayerExpertRotationKick";
            this._tbPlayerExpertRotationKick.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerExpertRotationKick.TabIndex = 7;
            // 
            // _tbPlayerExpertAcquireVelocity
            // 
            this._tbPlayerExpertAcquireVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerExpertAcquireVelocity.Location = new System.Drawing.Point(6, 51);
            this._tbPlayerExpertAcquireVelocity.Name = "_tbPlayerExpertAcquireVelocity";
            this._tbPlayerExpertAcquireVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerExpertAcquireVelocity.TabIndex = 1;
            // 
            // _tbPlayerExpertRotationStability
            // 
            this._tbPlayerExpertRotationStability.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerExpertRotationStability.Location = new System.Drawing.Point(6, 196);
            this._tbPlayerExpertRotationStability.Name = "_tbPlayerExpertRotationStability";
            this._tbPlayerExpertRotationStability.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerExpertRotationStability.TabIndex = 6;
            // 
            // _tbPlayerExpertTurnLagFactor
            // 
            this._tbPlayerExpertTurnLagFactor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerExpertTurnLagFactor.Location = new System.Drawing.Point(6, 167);
            this._tbPlayerExpertTurnLagFactor.Name = "_tbPlayerExpertTurnLagFactor";
            this._tbPlayerExpertTurnLagFactor.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerExpertTurnLagFactor.TabIndex = 5;
            // 
            // _tbPlayerExpertFrictionConstant
            // 
            this._tbPlayerExpertFrictionConstant.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerExpertFrictionConstant.Location = new System.Drawing.Point(6, 138);
            this._tbPlayerExpertFrictionConstant.Name = "_tbPlayerExpertFrictionConstant";
            this._tbPlayerExpertFrictionConstant.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerExpertFrictionConstant.TabIndex = 4;
            // 
            // _tbPlayerExpertTurnRate
            // 
            this._tbPlayerExpertTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerExpertTurnRate.Location = new System.Drawing.Point(6, 109);
            this._tbPlayerExpertTurnRate.Name = "_tbPlayerExpertTurnRate";
            this._tbPlayerExpertTurnRate.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerExpertTurnRate.TabIndex = 3;
            // 
            // _tbPlayerExpertNormalAccel
            // 
            this._tbPlayerExpertNormalAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerExpertNormalAccel.Location = new System.Drawing.Point(6, 80);
            this._tbPlayerExpertNormalAccel.Name = "_tbPlayerExpertNormalAccel";
            this._tbPlayerExpertNormalAccel.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerExpertNormalAccel.TabIndex = 2;
            // 
            // _tbPlayerExpertMaxVelocity
            // 
            this._tbPlayerExpertMaxVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerExpertMaxVelocity.Location = new System.Drawing.Point(6, 22);
            this._tbPlayerExpertMaxVelocity.Name = "_tbPlayerExpertMaxVelocity";
            this._tbPlayerExpertMaxVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerExpertMaxVelocity.TabIndex = 0;
            // 
            // _gbPlayerIntermediate
            // 
            this._gbPlayerIntermediate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._gbPlayerIntermediate.Controls.Add(this._tbPlayerIntermediateRotationAccel);
            this._gbPlayerIntermediate.Controls.Add(this._tbPlayerIntermediateRotationKick);
            this._gbPlayerIntermediate.Controls.Add(this._tbPlayerIntermediateRotationStability);
            this._gbPlayerIntermediate.Controls.Add(this._tbPlayerIntermediateAcquireVelocity);
            this._gbPlayerIntermediate.Controls.Add(this._tbPlayerIntermediateTurnLagFactor);
            this._gbPlayerIntermediate.Controls.Add(this._tbPlayerIntermediateFrictionConstant);
            this._gbPlayerIntermediate.Controls.Add(this._tbPlayerIntermediateTurnRate);
            this._gbPlayerIntermediate.Controls.Add(this._tbPlayerIntermediateNormalAccel);
            this._gbPlayerIntermediate.Controls.Add(this._tbPlayerIntermediateMaxVelocity);
            this._gbPlayerIntermediate.Location = new System.Drawing.Point(273, 6);
            this._gbPlayerIntermediate.Name = "_gbPlayerIntermediate";
            this._gbPlayerIntermediate.Size = new System.Drawing.Size(88, 283);
            this._gbPlayerIntermediate.TabIndex = 10;
            this._gbPlayerIntermediate.TabStop = false;
            this._gbPlayerIntermediate.Text = "Intermediate";
            // 
            // _tbPlayerIntermediateRotationAccel
            // 
            this._tbPlayerIntermediateRotationAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerIntermediateRotationAccel.Location = new System.Drawing.Point(6, 254);
            this._tbPlayerIntermediateRotationAccel.Name = "_tbPlayerIntermediateRotationAccel";
            this._tbPlayerIntermediateRotationAccel.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerIntermediateRotationAccel.TabIndex = 8;
            // 
            // _tbPlayerIntermediateRotationKick
            // 
            this._tbPlayerIntermediateRotationKick.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerIntermediateRotationKick.Location = new System.Drawing.Point(6, 225);
            this._tbPlayerIntermediateRotationKick.Name = "_tbPlayerIntermediateRotationKick";
            this._tbPlayerIntermediateRotationKick.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerIntermediateRotationKick.TabIndex = 7;
            // 
            // _tbPlayerIntermediateRotationStability
            // 
            this._tbPlayerIntermediateRotationStability.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerIntermediateRotationStability.Location = new System.Drawing.Point(6, 196);
            this._tbPlayerIntermediateRotationStability.Name = "_tbPlayerIntermediateRotationStability";
            this._tbPlayerIntermediateRotationStability.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerIntermediateRotationStability.TabIndex = 6;
            // 
            // _tbPlayerIntermediateAcquireVelocity
            // 
            this._tbPlayerIntermediateAcquireVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerIntermediateAcquireVelocity.Location = new System.Drawing.Point(6, 51);
            this._tbPlayerIntermediateAcquireVelocity.Name = "_tbPlayerIntermediateAcquireVelocity";
            this._tbPlayerIntermediateAcquireVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerIntermediateAcquireVelocity.TabIndex = 1;
            // 
            // _tbPlayerIntermediateTurnLagFactor
            // 
            this._tbPlayerIntermediateTurnLagFactor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerIntermediateTurnLagFactor.Location = new System.Drawing.Point(6, 167);
            this._tbPlayerIntermediateTurnLagFactor.Name = "_tbPlayerIntermediateTurnLagFactor";
            this._tbPlayerIntermediateTurnLagFactor.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerIntermediateTurnLagFactor.TabIndex = 5;
            // 
            // _tbPlayerIntermediateFrictionConstant
            // 
            this._tbPlayerIntermediateFrictionConstant.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerIntermediateFrictionConstant.Location = new System.Drawing.Point(6, 138);
            this._tbPlayerIntermediateFrictionConstant.Name = "_tbPlayerIntermediateFrictionConstant";
            this._tbPlayerIntermediateFrictionConstant.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerIntermediateFrictionConstant.TabIndex = 4;
            // 
            // _tbPlayerIntermediateTurnRate
            // 
            this._tbPlayerIntermediateTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerIntermediateTurnRate.Location = new System.Drawing.Point(6, 109);
            this._tbPlayerIntermediateTurnRate.Name = "_tbPlayerIntermediateTurnRate";
            this._tbPlayerIntermediateTurnRate.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerIntermediateTurnRate.TabIndex = 3;
            // 
            // _tbPlayerIntermediateNormalAccel
            // 
            this._tbPlayerIntermediateNormalAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerIntermediateNormalAccel.Location = new System.Drawing.Point(6, 80);
            this._tbPlayerIntermediateNormalAccel.Name = "_tbPlayerIntermediateNormalAccel";
            this._tbPlayerIntermediateNormalAccel.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerIntermediateNormalAccel.TabIndex = 2;
            // 
            // _tbPlayerIntermediateMaxVelocity
            // 
            this._tbPlayerIntermediateMaxVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerIntermediateMaxVelocity.Location = new System.Drawing.Point(6, 22);
            this._tbPlayerIntermediateMaxVelocity.Name = "_tbPlayerIntermediateMaxVelocity";
            this._tbPlayerIntermediateMaxVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerIntermediateMaxVelocity.TabIndex = 0;
            // 
            // _gbPlayerBeginner
            // 
            this._gbPlayerBeginner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._gbPlayerBeginner.Controls.Add(this._tbPlayerBeginnerRotationAccel);
            this._gbPlayerBeginner.Controls.Add(this._tbPlayerBeginnerRotationKick);
            this._gbPlayerBeginner.Controls.Add(this._tbPlayerBeginnerRotationStability);
            this._gbPlayerBeginner.Controls.Add(this._tbPlayerBeginnerTurnLagFactor);
            this._gbPlayerBeginner.Controls.Add(this._tbPlayerBeginnerAcquireVelocity);
            this._gbPlayerBeginner.Controls.Add(this._tbPlayerBeginnerFrictionConstant);
            this._gbPlayerBeginner.Controls.Add(this._tbPlayerBeginnerTurnRate);
            this._gbPlayerBeginner.Controls.Add(this._tbPlayerBeginnerNormalAccel);
            this._gbPlayerBeginner.Controls.Add(this._tbPlayerBeginnerMaxVelocity);
            this._gbPlayerBeginner.Location = new System.Drawing.Point(179, 6);
            this._gbPlayerBeginner.Name = "_gbPlayerBeginner";
            this._gbPlayerBeginner.Size = new System.Drawing.Size(88, 283);
            this._gbPlayerBeginner.TabIndex = 9;
            this._gbPlayerBeginner.TabStop = false;
            this._gbPlayerBeginner.Text = "Beginner";
            // 
            // _tbPlayerBeginnerRotationAccel
            // 
            this._tbPlayerBeginnerRotationAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerBeginnerRotationAccel.Location = new System.Drawing.Point(6, 254);
            this._tbPlayerBeginnerRotationAccel.Name = "_tbPlayerBeginnerRotationAccel";
            this._tbPlayerBeginnerRotationAccel.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerBeginnerRotationAccel.TabIndex = 8;
            // 
            // _tbPlayerBeginnerRotationKick
            // 
            this._tbPlayerBeginnerRotationKick.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerBeginnerRotationKick.Location = new System.Drawing.Point(6, 225);
            this._tbPlayerBeginnerRotationKick.Name = "_tbPlayerBeginnerRotationKick";
            this._tbPlayerBeginnerRotationKick.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerBeginnerRotationKick.TabIndex = 7;
            // 
            // _tbPlayerBeginnerRotationStability
            // 
            this._tbPlayerBeginnerRotationStability.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerBeginnerRotationStability.Location = new System.Drawing.Point(6, 196);
            this._tbPlayerBeginnerRotationStability.Name = "_tbPlayerBeginnerRotationStability";
            this._tbPlayerBeginnerRotationStability.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerBeginnerRotationStability.TabIndex = 6;
            // 
            // _tbPlayerBeginnerTurnLagFactor
            // 
            this._tbPlayerBeginnerTurnLagFactor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerBeginnerTurnLagFactor.Location = new System.Drawing.Point(6, 167);
            this._tbPlayerBeginnerTurnLagFactor.Name = "_tbPlayerBeginnerTurnLagFactor";
            this._tbPlayerBeginnerTurnLagFactor.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerBeginnerTurnLagFactor.TabIndex = 5;
            // 
            // _tbPlayerBeginnerAcquireVelocity
            // 
            this._tbPlayerBeginnerAcquireVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerBeginnerAcquireVelocity.Location = new System.Drawing.Point(6, 51);
            this._tbPlayerBeginnerAcquireVelocity.Name = "_tbPlayerBeginnerAcquireVelocity";
            this._tbPlayerBeginnerAcquireVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerBeginnerAcquireVelocity.TabIndex = 1;
            // 
            // _tbPlayerBeginnerFrictionConstant
            // 
            this._tbPlayerBeginnerFrictionConstant.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerBeginnerFrictionConstant.Location = new System.Drawing.Point(6, 138);
            this._tbPlayerBeginnerFrictionConstant.Name = "_tbPlayerBeginnerFrictionConstant";
            this._tbPlayerBeginnerFrictionConstant.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerBeginnerFrictionConstant.TabIndex = 4;
            // 
            // _tbPlayerBeginnerTurnRate
            // 
            this._tbPlayerBeginnerTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerBeginnerTurnRate.Location = new System.Drawing.Point(6, 109);
            this._tbPlayerBeginnerTurnRate.Name = "_tbPlayerBeginnerTurnRate";
            this._tbPlayerBeginnerTurnRate.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerBeginnerTurnRate.TabIndex = 3;
            // 
            // _tbPlayerBeginnerNormalAccel
            // 
            this._tbPlayerBeginnerNormalAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerBeginnerNormalAccel.Location = new System.Drawing.Point(6, 80);
            this._tbPlayerBeginnerNormalAccel.Name = "_tbPlayerBeginnerNormalAccel";
            this._tbPlayerBeginnerNormalAccel.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerBeginnerNormalAccel.TabIndex = 2;
            // 
            // _tbPlayerBeginnerMaxVelocity
            // 
            this._tbPlayerBeginnerMaxVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbPlayerBeginnerMaxVelocity.Location = new System.Drawing.Point(6, 22);
            this._tbPlayerBeginnerMaxVelocity.Name = "_tbPlayerBeginnerMaxVelocity";
            this._tbPlayerBeginnerMaxVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbPlayerBeginnerMaxVelocity.TabIndex = 0;
            // 
            // _tpDrone
            // 
            this._tpDrone.Controls.Add(this._gbDroneSpeedMultiplier);
            this._tpDrone.Controls.Add(this._lbDroneRotationAccel);
            this._tpDrone.Controls.Add(this._lbDroneRotationKick);
            this._tpDrone.Controls.Add(this._lbDroneRotationStability);
            this._tpDrone.Controls.Add(this._lbDroneTurnLagFactor);
            this._tpDrone.Controls.Add(this._lbDroneFrictionConstant);
            this._tpDrone.Controls.Add(this._lbDroneTurnRate);
            this._tpDrone.Controls.Add(this._lbDroneNormalAccel);
            this._tpDrone.Controls.Add(this._lbDroneAcquireVelocity);
            this._tpDrone.Controls.Add(this._lbDroneMaxVelocity);
            this._tpDrone.Controls.Add(this._gbDroneExpert);
            this._tpDrone.Controls.Add(this._gbDroneIntermediate);
            this._tpDrone.Controls.Add(this._gbDroneBeginner);
            this._tpDrone.Location = new System.Drawing.Point(4, 24);
            this._tpDrone.Name = "_tpDrone";
            this._tpDrone.Padding = new System.Windows.Forms.Padding(3);
            this._tpDrone.Size = new System.Drawing.Size(461, 381);
            this._tpDrone.TabIndex = 1;
            this._tpDrone.Text = "Drone Motion Physics";
            this._tpDrone.UseVisualStyleBackColor = true;
            // 
            // _gbDroneSpeedMultiplier
            // 
            this._gbDroneSpeedMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._gbDroneSpeedMultiplier.Controls.Add(this._lbDroneSpeedMultiplier);
            this._gbDroneSpeedMultiplier.Controls.Add(this._tbDroneExpertSlowMultiplier);
            this._gbDroneSpeedMultiplier.Controls.Add(this._tbDroneBeginnerSpeedMultiplier);
            this._gbDroneSpeedMultiplier.Controls.Add(this._tbDroneIntermediateSlowMultiplier);
            this._gbDroneSpeedMultiplier.Controls.Add(this._tbDroneIntermediateSpeedMultiplier);
            this._gbDroneSpeedMultiplier.Controls.Add(this._tbDroneBeginnerSlowMultiplier);
            this._gbDroneSpeedMultiplier.Controls.Add(this._tbDroneExpertSpeedMultiplier);
            this._gbDroneSpeedMultiplier.Controls.Add(this._lbDroneSlowMultiplier);
            this._gbDroneSpeedMultiplier.Location = new System.Drawing.Point(34, 295);
            this._gbDroneSpeedMultiplier.Name = "_gbDroneSpeedMultiplier";
            this._gbDroneSpeedMultiplier.Size = new System.Drawing.Size(421, 80);
            this._gbDroneSpeedMultiplier.TabIndex = 12;
            this._gbDroneSpeedMultiplier.TabStop = false;
            this._gbDroneSpeedMultiplier.Text = "Max Velocity Multipliers for Speed and Slow Pods";
            // 
            // _lbDroneSpeedMultiplier
            // 
            this._lbDroneSpeedMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbDroneSpeedMultiplier.AutoSize = true;
            this._lbDroneSpeedMultiplier.Location = new System.Drawing.Point(49, 25);
            this._lbDroneSpeedMultiplier.Name = "_lbDroneSpeedMultiplier";
            this._lbDroneSpeedMultiplier.Size = new System.Drawing.Size(96, 15);
            this._lbDroneSpeedMultiplier.TabIndex = 0;
            this._lbDroneSpeedMultiplier.Text = "Speed Multiplier:";
            // 
            // _tbDroneExpertSlowMultiplier
            // 
            this._tbDroneExpertSlowMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneExpertSlowMultiplier.Location = new System.Drawing.Point(339, 51);
            this._tbDroneExpertSlowMultiplier.Name = "_tbDroneExpertSlowMultiplier";
            this._tbDroneExpertSlowMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbDroneExpertSlowMultiplier.TabIndex = 7;
            // 
            // _tbDroneBeginnerSpeedMultiplier
            // 
            this._tbDroneBeginnerSpeedMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneBeginnerSpeedMultiplier.Location = new System.Drawing.Point(151, 22);
            this._tbDroneBeginnerSpeedMultiplier.Name = "_tbDroneBeginnerSpeedMultiplier";
            this._tbDroneBeginnerSpeedMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbDroneBeginnerSpeedMultiplier.TabIndex = 2;
            // 
            // _tbDroneIntermediateSlowMultiplier
            // 
            this._tbDroneIntermediateSlowMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneIntermediateSlowMultiplier.Location = new System.Drawing.Point(245, 51);
            this._tbDroneIntermediateSlowMultiplier.Name = "_tbDroneIntermediateSlowMultiplier";
            this._tbDroneIntermediateSlowMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbDroneIntermediateSlowMultiplier.TabIndex = 5;
            // 
            // _tbDroneIntermediateSpeedMultiplier
            // 
            this._tbDroneIntermediateSpeedMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneIntermediateSpeedMultiplier.Location = new System.Drawing.Point(245, 22);
            this._tbDroneIntermediateSpeedMultiplier.Name = "_tbDroneIntermediateSpeedMultiplier";
            this._tbDroneIntermediateSpeedMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbDroneIntermediateSpeedMultiplier.TabIndex = 4;
            // 
            // _tbDroneBeginnerSlowMultiplier
            // 
            this._tbDroneBeginnerSlowMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneBeginnerSlowMultiplier.Location = new System.Drawing.Point(151, 51);
            this._tbDroneBeginnerSlowMultiplier.Name = "_tbDroneBeginnerSlowMultiplier";
            this._tbDroneBeginnerSlowMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbDroneBeginnerSlowMultiplier.TabIndex = 3;
            // 
            // _tbDroneExpertSpeedMultiplier
            // 
            this._tbDroneExpertSpeedMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneExpertSpeedMultiplier.Location = new System.Drawing.Point(339, 22);
            this._tbDroneExpertSpeedMultiplier.Name = "_tbDroneExpertSpeedMultiplier";
            this._tbDroneExpertSpeedMultiplier.Size = new System.Drawing.Size(76, 23);
            this._tbDroneExpertSpeedMultiplier.TabIndex = 6;
            // 
            // _lbDroneSlowMultiplier
            // 
            this._lbDroneSlowMultiplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbDroneSlowMultiplier.AutoSize = true;
            this._lbDroneSlowMultiplier.Location = new System.Drawing.Point(6, 54);
            this._lbDroneSlowMultiplier.Name = "_lbDroneSlowMultiplier";
            this._lbDroneSlowMultiplier.Size = new System.Drawing.Size(139, 15);
            this._lbDroneSlowMultiplier.TabIndex = 1;
            this._lbDroneSlowMultiplier.Text = "Slow Multiplier (unused):";
            // 
            // _lbDroneRotationAccel
            // 
            this._lbDroneRotationAccel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbDroneRotationAccel.AutoSize = true;
            this._lbDroneRotationAccel.Location = new System.Drawing.Point(89, 263);
            this._lbDroneRotationAccel.Name = "_lbDroneRotationAccel";
            this._lbDroneRotationAccel.Size = new System.Drawing.Size(84, 15);
            this._lbDroneRotationAccel.TabIndex = 8;
            this._lbDroneRotationAccel.Text = "Rotation Accel";
            // 
            // _lbDroneRotationKick
            // 
            this._lbDroneRotationKick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbDroneRotationKick.AutoSize = true;
            this._lbDroneRotationKick.Location = new System.Drawing.Point(96, 234);
            this._lbDroneRotationKick.Name = "_lbDroneRotationKick";
            this._lbDroneRotationKick.Size = new System.Drawing.Size(77, 15);
            this._lbDroneRotationKick.TabIndex = 7;
            this._lbDroneRotationKick.Text = "Rotation Kick";
            // 
            // _lbDroneRotationStability
            // 
            this._lbDroneRotationStability.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbDroneRotationStability.AutoSize = true;
            this._lbDroneRotationStability.Location = new System.Drawing.Point(76, 205);
            this._lbDroneRotationStability.Name = "_lbDroneRotationStability";
            this._lbDroneRotationStability.Size = new System.Drawing.Size(97, 15);
            this._lbDroneRotationStability.TabIndex = 6;
            this._lbDroneRotationStability.Text = "Rotation Stability";
            // 
            // _lbDroneTurnLagFactor
            // 
            this._lbDroneTurnLagFactor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbDroneTurnLagFactor.AutoSize = true;
            this._lbDroneTurnLagFactor.Location = new System.Drawing.Point(83, 176);
            this._lbDroneTurnLagFactor.Name = "_lbDroneTurnLagFactor";
            this._lbDroneTurnLagFactor.Size = new System.Drawing.Size(90, 15);
            this._lbDroneTurnLagFactor.TabIndex = 5;
            this._lbDroneTurnLagFactor.Text = "Turn Lag Factor";
            // 
            // _lbDroneFrictionConstant
            // 
            this._lbDroneFrictionConstant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbDroneFrictionConstant.AutoSize = true;
            this._lbDroneFrictionConstant.Location = new System.Drawing.Point(75, 147);
            this._lbDroneFrictionConstant.Name = "_lbDroneFrictionConstant";
            this._lbDroneFrictionConstant.Size = new System.Drawing.Size(98, 15);
            this._lbDroneFrictionConstant.TabIndex = 4;
            this._lbDroneFrictionConstant.Text = "Friction Constant";
            // 
            // _lbDroneTurnRate
            // 
            this._lbDroneTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbDroneTurnRate.AutoSize = true;
            this._lbDroneTurnRate.Location = new System.Drawing.Point(115, 118);
            this._lbDroneTurnRate.Name = "_lbDroneTurnRate";
            this._lbDroneTurnRate.Size = new System.Drawing.Size(58, 15);
            this._lbDroneTurnRate.TabIndex = 3;
            this._lbDroneTurnRate.Text = "Turn Rate";
            // 
            // _lbDroneNormalAccel
            // 
            this._lbDroneNormalAccel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbDroneNormalAccel.AutoSize = true;
            this._lbDroneNormalAccel.Location = new System.Drawing.Point(94, 89);
            this._lbDroneNormalAccel.Name = "_lbDroneNormalAccel";
            this._lbDroneNormalAccel.Size = new System.Drawing.Size(79, 15);
            this._lbDroneNormalAccel.TabIndex = 2;
            this._lbDroneNormalAccel.Text = "Normal Accel";
            // 
            // _lbDroneAcquireVelocity
            // 
            this._lbDroneAcquireVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbDroneAcquireVelocity.AutoSize = true;
            this._lbDroneAcquireVelocity.Location = new System.Drawing.Point(81, 60);
            this._lbDroneAcquireVelocity.Name = "_lbDroneAcquireVelocity";
            this._lbDroneAcquireVelocity.Size = new System.Drawing.Size(92, 15);
            this._lbDroneAcquireVelocity.TabIndex = 1;
            this._lbDroneAcquireVelocity.Text = "Acquire Velocity";
            // 
            // _lbDroneMaxVelocity
            // 
            this._lbDroneMaxVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbDroneMaxVelocity.AutoSize = true;
            this._lbDroneMaxVelocity.Location = new System.Drawing.Point(100, 31);
            this._lbDroneMaxVelocity.Name = "_lbDroneMaxVelocity";
            this._lbDroneMaxVelocity.Size = new System.Drawing.Size(73, 15);
            this._lbDroneMaxVelocity.TabIndex = 0;
            this._lbDroneMaxVelocity.Text = "Max Velocity";
            // 
            // _gbDroneExpert
            // 
            this._gbDroneExpert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._gbDroneExpert.Controls.Add(this._tbDroneExpertRotationAccel);
            this._gbDroneExpert.Controls.Add(this._tbDroneExpertMaxVelocity);
            this._gbDroneExpert.Controls.Add(this._tbDroneExpertFrictionConstant);
            this._gbDroneExpert.Controls.Add(this._tbDroneExpertRotationKick);
            this._gbDroneExpert.Controls.Add(this._tbDroneExpertTurnRate);
            this._gbDroneExpert.Controls.Add(this._tbDroneExpertAquireVelocity);
            this._gbDroneExpert.Controls.Add(this._tbDroneExpertTurnLagFactor);
            this._gbDroneExpert.Controls.Add(this._tbDroneExpertRotationStability);
            this._gbDroneExpert.Controls.Add(this._tbDroneExpertNormalAccel);
            this._gbDroneExpert.Location = new System.Drawing.Point(367, 6);
            this._gbDroneExpert.Name = "_gbDroneExpert";
            this._gbDroneExpert.Size = new System.Drawing.Size(88, 283);
            this._gbDroneExpert.TabIndex = 11;
            this._gbDroneExpert.TabStop = false;
            this._gbDroneExpert.Text = "Expert";
            // 
            // _tbDroneExpertRotationAccel
            // 
            this._tbDroneExpertRotationAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneExpertRotationAccel.Location = new System.Drawing.Point(6, 254);
            this._tbDroneExpertRotationAccel.Name = "_tbDroneExpertRotationAccel";
            this._tbDroneExpertRotationAccel.Size = new System.Drawing.Size(76, 23);
            this._tbDroneExpertRotationAccel.TabIndex = 8;
            // 
            // _tbDroneExpertMaxVelocity
            // 
            this._tbDroneExpertMaxVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneExpertMaxVelocity.Location = new System.Drawing.Point(6, 22);
            this._tbDroneExpertMaxVelocity.Name = "_tbDroneExpertMaxVelocity";
            this._tbDroneExpertMaxVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbDroneExpertMaxVelocity.TabIndex = 0;
            // 
            // _tbDroneExpertFrictionConstant
            // 
            this._tbDroneExpertFrictionConstant.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneExpertFrictionConstant.Location = new System.Drawing.Point(6, 138);
            this._tbDroneExpertFrictionConstant.Name = "_tbDroneExpertFrictionConstant";
            this._tbDroneExpertFrictionConstant.Size = new System.Drawing.Size(76, 23);
            this._tbDroneExpertFrictionConstant.TabIndex = 4;
            // 
            // _tbDroneExpertRotationKick
            // 
            this._tbDroneExpertRotationKick.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneExpertRotationKick.Location = new System.Drawing.Point(6, 225);
            this._tbDroneExpertRotationKick.Name = "_tbDroneExpertRotationKick";
            this._tbDroneExpertRotationKick.Size = new System.Drawing.Size(76, 23);
            this._tbDroneExpertRotationKick.TabIndex = 7;
            // 
            // _tbDroneExpertTurnRate
            // 
            this._tbDroneExpertTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneExpertTurnRate.Location = new System.Drawing.Point(6, 109);
            this._tbDroneExpertTurnRate.Name = "_tbDroneExpertTurnRate";
            this._tbDroneExpertTurnRate.Size = new System.Drawing.Size(76, 23);
            this._tbDroneExpertTurnRate.TabIndex = 3;
            // 
            // _tbDroneExpertAquireVelocity
            // 
            this._tbDroneExpertAquireVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneExpertAquireVelocity.Location = new System.Drawing.Point(6, 51);
            this._tbDroneExpertAquireVelocity.Name = "_tbDroneExpertAquireVelocity";
            this._tbDroneExpertAquireVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbDroneExpertAquireVelocity.TabIndex = 1;
            // 
            // _tbDroneExpertTurnLagFactor
            // 
            this._tbDroneExpertTurnLagFactor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneExpertTurnLagFactor.Location = new System.Drawing.Point(6, 167);
            this._tbDroneExpertTurnLagFactor.Name = "_tbDroneExpertTurnLagFactor";
            this._tbDroneExpertTurnLagFactor.Size = new System.Drawing.Size(76, 23);
            this._tbDroneExpertTurnLagFactor.TabIndex = 5;
            // 
            // _tbDroneExpertRotationStability
            // 
            this._tbDroneExpertRotationStability.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneExpertRotationStability.Location = new System.Drawing.Point(6, 196);
            this._tbDroneExpertRotationStability.Name = "_tbDroneExpertRotationStability";
            this._tbDroneExpertRotationStability.Size = new System.Drawing.Size(76, 23);
            this._tbDroneExpertRotationStability.TabIndex = 6;
            // 
            // _tbDroneExpertNormalAccel
            // 
            this._tbDroneExpertNormalAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneExpertNormalAccel.Location = new System.Drawing.Point(6, 80);
            this._tbDroneExpertNormalAccel.Name = "_tbDroneExpertNormalAccel";
            this._tbDroneExpertNormalAccel.Size = new System.Drawing.Size(76, 23);
            this._tbDroneExpertNormalAccel.TabIndex = 2;
            // 
            // _gbDroneIntermediate
            // 
            this._gbDroneIntermediate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._gbDroneIntermediate.Controls.Add(this._tbDroneIntermediateRotationAccel);
            this._gbDroneIntermediate.Controls.Add(this._tbDroneIntermediateMaxVelocity);
            this._gbDroneIntermediate.Controls.Add(this._tbDroneIntermediateRotationKick);
            this._gbDroneIntermediate.Controls.Add(this._tbDroneIntermediateAquireVelocity);
            this._gbDroneIntermediate.Controls.Add(this._tbDroneIntermediateRotationStability);
            this._gbDroneIntermediate.Controls.Add(this._tbDroneIntermediateNormalAccel);
            this._gbDroneIntermediate.Controls.Add(this._tbDroneIntermediateTurnLagFactor);
            this._gbDroneIntermediate.Controls.Add(this._tbDroneIntermediateTurnRate);
            this._gbDroneIntermediate.Controls.Add(this._tbDroneIntermediateFrictionConstant);
            this._gbDroneIntermediate.Location = new System.Drawing.Point(273, 6);
            this._gbDroneIntermediate.Name = "_gbDroneIntermediate";
            this._gbDroneIntermediate.Size = new System.Drawing.Size(88, 283);
            this._gbDroneIntermediate.TabIndex = 10;
            this._gbDroneIntermediate.TabStop = false;
            this._gbDroneIntermediate.Text = "Intermediate";
            // 
            // _tbDroneIntermediateRotationAccel
            // 
            this._tbDroneIntermediateRotationAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneIntermediateRotationAccel.Location = new System.Drawing.Point(6, 254);
            this._tbDroneIntermediateRotationAccel.Name = "_tbDroneIntermediateRotationAccel";
            this._tbDroneIntermediateRotationAccel.Size = new System.Drawing.Size(76, 23);
            this._tbDroneIntermediateRotationAccel.TabIndex = 8;
            // 
            // _tbDroneIntermediateMaxVelocity
            // 
            this._tbDroneIntermediateMaxVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneIntermediateMaxVelocity.Location = new System.Drawing.Point(6, 22);
            this._tbDroneIntermediateMaxVelocity.Name = "_tbDroneIntermediateMaxVelocity";
            this._tbDroneIntermediateMaxVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbDroneIntermediateMaxVelocity.TabIndex = 0;
            // 
            // _tbDroneIntermediateRotationKick
            // 
            this._tbDroneIntermediateRotationKick.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneIntermediateRotationKick.Location = new System.Drawing.Point(6, 225);
            this._tbDroneIntermediateRotationKick.Name = "_tbDroneIntermediateRotationKick";
            this._tbDroneIntermediateRotationKick.Size = new System.Drawing.Size(76, 23);
            this._tbDroneIntermediateRotationKick.TabIndex = 7;
            // 
            // _tbDroneIntermediateAquireVelocity
            // 
            this._tbDroneIntermediateAquireVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneIntermediateAquireVelocity.Location = new System.Drawing.Point(6, 51);
            this._tbDroneIntermediateAquireVelocity.Name = "_tbDroneIntermediateAquireVelocity";
            this._tbDroneIntermediateAquireVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbDroneIntermediateAquireVelocity.TabIndex = 1;
            // 
            // _tbDroneIntermediateRotationStability
            // 
            this._tbDroneIntermediateRotationStability.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneIntermediateRotationStability.Location = new System.Drawing.Point(6, 196);
            this._tbDroneIntermediateRotationStability.Name = "_tbDroneIntermediateRotationStability";
            this._tbDroneIntermediateRotationStability.Size = new System.Drawing.Size(76, 23);
            this._tbDroneIntermediateRotationStability.TabIndex = 6;
            // 
            // _tbDroneIntermediateNormalAccel
            // 
            this._tbDroneIntermediateNormalAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneIntermediateNormalAccel.Location = new System.Drawing.Point(6, 80);
            this._tbDroneIntermediateNormalAccel.Name = "_tbDroneIntermediateNormalAccel";
            this._tbDroneIntermediateNormalAccel.Size = new System.Drawing.Size(76, 23);
            this._tbDroneIntermediateNormalAccel.TabIndex = 2;
            // 
            // _tbDroneIntermediateTurnLagFactor
            // 
            this._tbDroneIntermediateTurnLagFactor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneIntermediateTurnLagFactor.Location = new System.Drawing.Point(6, 167);
            this._tbDroneIntermediateTurnLagFactor.Name = "_tbDroneIntermediateTurnLagFactor";
            this._tbDroneIntermediateTurnLagFactor.Size = new System.Drawing.Size(76, 23);
            this._tbDroneIntermediateTurnLagFactor.TabIndex = 5;
            // 
            // _tbDroneIntermediateTurnRate
            // 
            this._tbDroneIntermediateTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneIntermediateTurnRate.Location = new System.Drawing.Point(6, 109);
            this._tbDroneIntermediateTurnRate.Name = "_tbDroneIntermediateTurnRate";
            this._tbDroneIntermediateTurnRate.Size = new System.Drawing.Size(76, 23);
            this._tbDroneIntermediateTurnRate.TabIndex = 3;
            // 
            // _tbDroneIntermediateFrictionConstant
            // 
            this._tbDroneIntermediateFrictionConstant.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneIntermediateFrictionConstant.Location = new System.Drawing.Point(6, 138);
            this._tbDroneIntermediateFrictionConstant.Name = "_tbDroneIntermediateFrictionConstant";
            this._tbDroneIntermediateFrictionConstant.Size = new System.Drawing.Size(76, 23);
            this._tbDroneIntermediateFrictionConstant.TabIndex = 4;
            // 
            // _gbDroneBeginner
            // 
            this._gbDroneBeginner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._gbDroneBeginner.Controls.Add(this._tbDroneBeginnerRotationAccel);
            this._gbDroneBeginner.Controls.Add(this._tbDroneBeginnerRotationKick);
            this._gbDroneBeginner.Controls.Add(this._tbDroneBeginnerRotationStability);
            this._gbDroneBeginner.Controls.Add(this._tbDroneBeginnerTurnLagFactor);
            this._gbDroneBeginner.Controls.Add(this._tbDroneBeginnerFrictionConstant);
            this._gbDroneBeginner.Controls.Add(this._tbDroneBeginnerTurnRate);
            this._gbDroneBeginner.Controls.Add(this._tbDroneBeginnerNormalAccel);
            this._gbDroneBeginner.Controls.Add(this._tbDroneBeginnerAquireVelocity);
            this._gbDroneBeginner.Controls.Add(this._tbDroneBeginnerMaxVelocity);
            this._gbDroneBeginner.Location = new System.Drawing.Point(179, 6);
            this._gbDroneBeginner.Name = "_gbDroneBeginner";
            this._gbDroneBeginner.Size = new System.Drawing.Size(88, 283);
            this._gbDroneBeginner.TabIndex = 9;
            this._gbDroneBeginner.TabStop = false;
            this._gbDroneBeginner.Text = "Beginner";
            // 
            // _tbDroneBeginnerRotationAccel
            // 
            this._tbDroneBeginnerRotationAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneBeginnerRotationAccel.Location = new System.Drawing.Point(6, 254);
            this._tbDroneBeginnerRotationAccel.Name = "_tbDroneBeginnerRotationAccel";
            this._tbDroneBeginnerRotationAccel.Size = new System.Drawing.Size(76, 23);
            this._tbDroneBeginnerRotationAccel.TabIndex = 8;
            // 
            // _tbDroneBeginnerRotationKick
            // 
            this._tbDroneBeginnerRotationKick.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneBeginnerRotationKick.Location = new System.Drawing.Point(6, 225);
            this._tbDroneBeginnerRotationKick.Name = "_tbDroneBeginnerRotationKick";
            this._tbDroneBeginnerRotationKick.Size = new System.Drawing.Size(76, 23);
            this._tbDroneBeginnerRotationKick.TabIndex = 7;
            // 
            // _tbDroneBeginnerRotationStability
            // 
            this._tbDroneBeginnerRotationStability.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneBeginnerRotationStability.Location = new System.Drawing.Point(6, 196);
            this._tbDroneBeginnerRotationStability.Name = "_tbDroneBeginnerRotationStability";
            this._tbDroneBeginnerRotationStability.Size = new System.Drawing.Size(76, 23);
            this._tbDroneBeginnerRotationStability.TabIndex = 6;
            // 
            // _tbDroneBeginnerTurnLagFactor
            // 
            this._tbDroneBeginnerTurnLagFactor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneBeginnerTurnLagFactor.Location = new System.Drawing.Point(6, 167);
            this._tbDroneBeginnerTurnLagFactor.Name = "_tbDroneBeginnerTurnLagFactor";
            this._tbDroneBeginnerTurnLagFactor.Size = new System.Drawing.Size(76, 23);
            this._tbDroneBeginnerTurnLagFactor.TabIndex = 5;
            // 
            // _tbDroneBeginnerFrictionConstant
            // 
            this._tbDroneBeginnerFrictionConstant.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneBeginnerFrictionConstant.Location = new System.Drawing.Point(6, 138);
            this._tbDroneBeginnerFrictionConstant.Name = "_tbDroneBeginnerFrictionConstant";
            this._tbDroneBeginnerFrictionConstant.Size = new System.Drawing.Size(76, 23);
            this._tbDroneBeginnerFrictionConstant.TabIndex = 4;
            // 
            // _tbDroneBeginnerTurnRate
            // 
            this._tbDroneBeginnerTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneBeginnerTurnRate.Location = new System.Drawing.Point(6, 109);
            this._tbDroneBeginnerTurnRate.Name = "_tbDroneBeginnerTurnRate";
            this._tbDroneBeginnerTurnRate.Size = new System.Drawing.Size(76, 23);
            this._tbDroneBeginnerTurnRate.TabIndex = 3;
            // 
            // _tbDroneBeginnerNormalAccel
            // 
            this._tbDroneBeginnerNormalAccel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneBeginnerNormalAccel.Location = new System.Drawing.Point(6, 80);
            this._tbDroneBeginnerNormalAccel.Name = "_tbDroneBeginnerNormalAccel";
            this._tbDroneBeginnerNormalAccel.Size = new System.Drawing.Size(76, 23);
            this._tbDroneBeginnerNormalAccel.TabIndex = 2;
            // 
            // _tbDroneBeginnerAquireVelocity
            // 
            this._tbDroneBeginnerAquireVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneBeginnerAquireVelocity.Location = new System.Drawing.Point(6, 51);
            this._tbDroneBeginnerAquireVelocity.Name = "_tbDroneBeginnerAquireVelocity";
            this._tbDroneBeginnerAquireVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbDroneBeginnerAquireVelocity.TabIndex = 1;
            // 
            // _tbDroneBeginnerMaxVelocity
            // 
            this._tbDroneBeginnerMaxVelocity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbDroneBeginnerMaxVelocity.Location = new System.Drawing.Point(6, 22);
            this._tbDroneBeginnerMaxVelocity.Name = "_tbDroneBeginnerMaxVelocity";
            this._tbDroneBeginnerMaxVelocity.Size = new System.Drawing.Size(76, 23);
            this._tbDroneBeginnerMaxVelocity.TabIndex = 0;
            // 
            // _tpAI
            // 
            this._tpAI.Controls.Add(this._lbAIBeaconSearchDepth);
            this._tpAI.Controls.Add(this._lbAIMaxBeaconHeightDifference);
            this._tpAI.Controls.Add(this._lbAIMaxBeaconDistance);
            this._tpAI.Controls.Add(this._lbAIMaxDistanceToAcquireFlag);
            this._tpAI.Controls.Add(this._lbAIMaxDistToAcquireHuman);
            this._tpAI.Controls.Add(this._lbAIMaxAngleForAcceleration);
            this._tpAI.Controls.Add(this._lbAIMaxTargetAngleAdjust);
            this._tpAI.Controls.Add(this._lbAIBackupSpeedFactor);
            this._tpAI.Controls.Add(this._lbAIBackupGameCycles);
            this._tpAI.Controls.Add(this._lbAIBackupTurnRate);
            this._tpAI.Controls.Add(this._gbAIExpert);
            this._tpAI.Controls.Add(this._gbAIIntermediate);
            this._tpAI.Controls.Add(this._gbAIBeginner);
            this._tpAI.Location = new System.Drawing.Point(4, 24);
            this._tpAI.Name = "_tpAI";
            this._tpAI.Padding = new System.Windows.Forms.Padding(3);
            this._tpAI.Size = new System.Drawing.Size(461, 381);
            this._tpAI.TabIndex = 2;
            this._tpAI.Text = "Robot AI Tuning Parameters";
            this._tpAI.UseVisualStyleBackColor = true;
            // 
            // _lbAIBeaconSearchDepth
            // 
            this._lbAIBeaconSearchDepth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbAIBeaconSearchDepth.AutoSize = true;
            this._lbAIBeaconSearchDepth.Location = new System.Drawing.Point(54, 292);
            this._lbAIBeaconSearchDepth.Name = "_lbAIBeaconSearchDepth";
            this._lbAIBeaconSearchDepth.Size = new System.Drawing.Size(119, 15);
            this._lbAIBeaconSearchDepth.TabIndex = 9;
            this._lbAIBeaconSearchDepth.Text = "Beacon Search Depth";
            // 
            // _lbAIMaxBeaconHeightDifference
            // 
            this._lbAIMaxBeaconHeightDifference.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbAIMaxBeaconHeightDifference.AutoSize = true;
            this._lbAIMaxBeaconHeightDifference.Location = new System.Drawing.Point(6, 263);
            this._lbAIMaxBeaconHeightDifference.Name = "_lbAIMaxBeaconHeightDifference";
            this._lbAIMaxBeaconHeightDifference.Size = new System.Drawing.Size(167, 15);
            this._lbAIMaxBeaconHeightDifference.TabIndex = 8;
            this._lbAIMaxBeaconHeightDifference.Text = "Max Beacon Height Difference";
            // 
            // _lbAIMaxBeaconDistance
            // 
            this._lbAIMaxBeaconDistance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbAIMaxBeaconDistance.AutoSize = true;
            this._lbAIMaxBeaconDistance.Location = new System.Drawing.Point(54, 234);
            this._lbAIMaxBeaconDistance.Name = "_lbAIMaxBeaconDistance";
            this._lbAIMaxBeaconDistance.Size = new System.Drawing.Size(119, 15);
            this._lbAIMaxBeaconDistance.TabIndex = 7;
            this._lbAIMaxBeaconDistance.Text = "Max Beacon Distance";
            // 
            // _lbAIMaxDistanceToAcquireFlag
            // 
            this._lbAIMaxDistanceToAcquireFlag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbAIMaxDistanceToAcquireFlag.AutoSize = true;
            this._lbAIMaxDistanceToAcquireFlag.Location = new System.Drawing.Point(38, 205);
            this._lbAIMaxDistanceToAcquireFlag.Name = "_lbAIMaxDistanceToAcquireFlag";
            this._lbAIMaxDistanceToAcquireFlag.Size = new System.Drawing.Size(135, 15);
            this._lbAIMaxDistanceToAcquireFlag.TabIndex = 6;
            this._lbAIMaxDistanceToAcquireFlag.Text = "Max Dist to Acquire Flag";
            // 
            // _lbAIMaxDistToAcquireHuman
            // 
            this._lbAIMaxDistToAcquireHuman.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbAIMaxDistToAcquireHuman.AutoSize = true;
            this._lbAIMaxDistToAcquireHuman.Location = new System.Drawing.Point(20, 176);
            this._lbAIMaxDistToAcquireHuman.Name = "_lbAIMaxDistToAcquireHuman";
            this._lbAIMaxDistToAcquireHuman.Size = new System.Drawing.Size(153, 15);
            this._lbAIMaxDistToAcquireHuman.TabIndex = 5;
            this._lbAIMaxDistToAcquireHuman.Text = "Max Dist to Acquire Human";
            // 
            // _lbAIMaxAngleForAcceleration
            // 
            this._lbAIMaxAngleForAcceleration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbAIMaxAngleForAcceleration.AutoSize = true;
            this._lbAIMaxAngleForAcceleration.Location = new System.Drawing.Point(23, 147);
            this._lbAIMaxAngleForAcceleration.Name = "_lbAIMaxAngleForAcceleration";
            this._lbAIMaxAngleForAcceleration.Size = new System.Drawing.Size(150, 15);
            this._lbAIMaxAngleForAcceleration.TabIndex = 4;
            this._lbAIMaxAngleForAcceleration.Text = "Max Angle for Acceleration";
            // 
            // _lbAIMaxTargetAngleAdjust
            // 
            this._lbAIMaxTargetAngleAdjust.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbAIMaxTargetAngleAdjust.AutoSize = true;
            this._lbAIMaxTargetAngleAdjust.Location = new System.Drawing.Point(37, 118);
            this._lbAIMaxTargetAngleAdjust.Name = "_lbAIMaxTargetAngleAdjust";
            this._lbAIMaxTargetAngleAdjust.Size = new System.Drawing.Size(136, 15);
            this._lbAIMaxTargetAngleAdjust.TabIndex = 3;
            this._lbAIMaxTargetAngleAdjust.Text = "Max Target Angle Adjust";
            // 
            // _lbAIBackupSpeedFactor
            // 
            this._lbAIBackupSpeedFactor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbAIBackupSpeedFactor.AutoSize = true;
            this._lbAIBackupSpeedFactor.Location = new System.Drawing.Point(56, 89);
            this._lbAIBackupSpeedFactor.Name = "_lbAIBackupSpeedFactor";
            this._lbAIBackupSpeedFactor.Size = new System.Drawing.Size(117, 15);
            this._lbAIBackupSpeedFactor.TabIndex = 2;
            this._lbAIBackupSpeedFactor.Text = "Backup Speed Factor";
            // 
            // _lbAIBackupGameCycles
            // 
            this._lbAIBackupGameCycles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbAIBackupGameCycles.AutoSize = true;
            this._lbAIBackupGameCycles.Location = new System.Drawing.Point(56, 60);
            this._lbAIBackupGameCycles.Name = "_lbAIBackupGameCycles";
            this._lbAIBackupGameCycles.Size = new System.Drawing.Size(117, 15);
            this._lbAIBackupGameCycles.TabIndex = 1;
            this._lbAIBackupGameCycles.Text = "Backup Game Cycles";
            // 
            // _lbAIBackupTurnRate
            // 
            this._lbAIBackupTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lbAIBackupTurnRate.AutoSize = true;
            this._lbAIBackupTurnRate.Location = new System.Drawing.Point(73, 31);
            this._lbAIBackupTurnRate.Name = "_lbAIBackupTurnRate";
            this._lbAIBackupTurnRate.Size = new System.Drawing.Size(100, 15);
            this._lbAIBackupTurnRate.TabIndex = 0;
            this._lbAIBackupTurnRate.Text = "Backup Turn Rate";
            // 
            // _gbAIExpert
            // 
            this._gbAIExpert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._gbAIExpert.Controls.Add(this._tbAIExpertBeaconSearchDepth);
            this._gbAIExpert.Controls.Add(this._tbAIExpertMaxBeaconHeightDifference);
            this._gbAIExpert.Controls.Add(this._tbAIExpertBackupTurnRate);
            this._gbAIExpert.Controls.Add(this._tbAIExpertMaxAngleForAcceleration);
            this._gbAIExpert.Controls.Add(this._tbAIExpertMaxBeaconDistance);
            this._gbAIExpert.Controls.Add(this._tbAIExpertMaxTargetAngleAdjust);
            this._gbAIExpert.Controls.Add(this._tbAIExpertBackupGameCycles);
            this._gbAIExpert.Controls.Add(this._tbAIExpertMaxDistToAcquireHuman);
            this._gbAIExpert.Controls.Add(this._tbAIExpertMaxDistToAcquireFlag);
            this._gbAIExpert.Controls.Add(this._tbAIExpertBackupSpeedFactor);
            this._gbAIExpert.Location = new System.Drawing.Point(367, 6);
            this._gbAIExpert.Name = "_gbAIExpert";
            this._gbAIExpert.Size = new System.Drawing.Size(88, 312);
            this._gbAIExpert.TabIndex = 12;
            this._gbAIExpert.TabStop = false;
            this._gbAIExpert.Text = "Expert";
            // 
            // _tbAIExpertBeaconSearchDepth
            // 
            this._tbAIExpertBeaconSearchDepth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIExpertBeaconSearchDepth.Location = new System.Drawing.Point(6, 283);
            this._tbAIExpertBeaconSearchDepth.Name = "_tbAIExpertBeaconSearchDepth";
            this._tbAIExpertBeaconSearchDepth.Size = new System.Drawing.Size(76, 23);
            this._tbAIExpertBeaconSearchDepth.TabIndex = 9;
            // 
            // _tbAIExpertMaxBeaconHeightDifference
            // 
            this._tbAIExpertMaxBeaconHeightDifference.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIExpertMaxBeaconHeightDifference.Location = new System.Drawing.Point(6, 254);
            this._tbAIExpertMaxBeaconHeightDifference.Name = "_tbAIExpertMaxBeaconHeightDifference";
            this._tbAIExpertMaxBeaconHeightDifference.Size = new System.Drawing.Size(76, 23);
            this._tbAIExpertMaxBeaconHeightDifference.TabIndex = 8;
            // 
            // _tbAIExpertBackupTurnRate
            // 
            this._tbAIExpertBackupTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIExpertBackupTurnRate.Location = new System.Drawing.Point(6, 22);
            this._tbAIExpertBackupTurnRate.Name = "_tbAIExpertBackupTurnRate";
            this._tbAIExpertBackupTurnRate.Size = new System.Drawing.Size(76, 23);
            this._tbAIExpertBackupTurnRate.TabIndex = 0;
            // 
            // _tbAIExpertMaxAngleForAcceleration
            // 
            this._tbAIExpertMaxAngleForAcceleration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIExpertMaxAngleForAcceleration.Location = new System.Drawing.Point(6, 138);
            this._tbAIExpertMaxAngleForAcceleration.Name = "_tbAIExpertMaxAngleForAcceleration";
            this._tbAIExpertMaxAngleForAcceleration.Size = new System.Drawing.Size(76, 23);
            this._tbAIExpertMaxAngleForAcceleration.TabIndex = 4;
            // 
            // _tbAIExpertMaxBeaconDistance
            // 
            this._tbAIExpertMaxBeaconDistance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIExpertMaxBeaconDistance.Location = new System.Drawing.Point(6, 225);
            this._tbAIExpertMaxBeaconDistance.Name = "_tbAIExpertMaxBeaconDistance";
            this._tbAIExpertMaxBeaconDistance.Size = new System.Drawing.Size(76, 23);
            this._tbAIExpertMaxBeaconDistance.TabIndex = 7;
            // 
            // _tbAIExpertMaxTargetAngleAdjust
            // 
            this._tbAIExpertMaxTargetAngleAdjust.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIExpertMaxTargetAngleAdjust.Location = new System.Drawing.Point(6, 109);
            this._tbAIExpertMaxTargetAngleAdjust.Name = "_tbAIExpertMaxTargetAngleAdjust";
            this._tbAIExpertMaxTargetAngleAdjust.Size = new System.Drawing.Size(76, 23);
            this._tbAIExpertMaxTargetAngleAdjust.TabIndex = 3;
            // 
            // _tbAIExpertBackupGameCycles
            // 
            this._tbAIExpertBackupGameCycles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIExpertBackupGameCycles.Location = new System.Drawing.Point(6, 51);
            this._tbAIExpertBackupGameCycles.Name = "_tbAIExpertBackupGameCycles";
            this._tbAIExpertBackupGameCycles.Size = new System.Drawing.Size(76, 23);
            this._tbAIExpertBackupGameCycles.TabIndex = 1;
            // 
            // _tbAIExpertMaxDistToAcquireHuman
            // 
            this._tbAIExpertMaxDistToAcquireHuman.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIExpertMaxDistToAcquireHuman.Location = new System.Drawing.Point(6, 167);
            this._tbAIExpertMaxDistToAcquireHuman.Name = "_tbAIExpertMaxDistToAcquireHuman";
            this._tbAIExpertMaxDistToAcquireHuman.Size = new System.Drawing.Size(76, 23);
            this._tbAIExpertMaxDistToAcquireHuman.TabIndex = 5;
            // 
            // _tbAIExpertMaxDistToAcquireFlag
            // 
            this._tbAIExpertMaxDistToAcquireFlag.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIExpertMaxDistToAcquireFlag.Location = new System.Drawing.Point(6, 196);
            this._tbAIExpertMaxDistToAcquireFlag.Name = "_tbAIExpertMaxDistToAcquireFlag";
            this._tbAIExpertMaxDistToAcquireFlag.Size = new System.Drawing.Size(76, 23);
            this._tbAIExpertMaxDistToAcquireFlag.TabIndex = 6;
            // 
            // _tbAIExpertBackupSpeedFactor
            // 
            this._tbAIExpertBackupSpeedFactor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIExpertBackupSpeedFactor.Location = new System.Drawing.Point(6, 80);
            this._tbAIExpertBackupSpeedFactor.Name = "_tbAIExpertBackupSpeedFactor";
            this._tbAIExpertBackupSpeedFactor.Size = new System.Drawing.Size(76, 23);
            this._tbAIExpertBackupSpeedFactor.TabIndex = 2;
            // 
            // _gbAIIntermediate
            // 
            this._gbAIIntermediate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._gbAIIntermediate.Controls.Add(this._tbAIIntermediateBeaconSearchDepth);
            this._gbAIIntermediate.Controls.Add(this._tbAIIntermediateMaxBeaconHeightDifference);
            this._gbAIIntermediate.Controls.Add(this._tbAIIntermediateBackupTurnRate);
            this._gbAIIntermediate.Controls.Add(this._tbAIIntermediateMaxBeaconDistance);
            this._gbAIIntermediate.Controls.Add(this._tbAIIntermediateBackupGameCycles);
            this._gbAIIntermediate.Controls.Add(this._tbAIIntermediateMaxDistToAcquireFlag);
            this._gbAIIntermediate.Controls.Add(this._tbAIIntermediateBackupSpeedFactor);
            this._gbAIIntermediate.Controls.Add(this._tbAIIntermediateMaxDistToAcquireHuman);
            this._gbAIIntermediate.Controls.Add(this._tbAIIntermediateMaxTargetAngleAdjust);
            this._gbAIIntermediate.Controls.Add(this._tbAIIntermediateMaxAngleForAcceleration);
            this._gbAIIntermediate.Location = new System.Drawing.Point(273, 6);
            this._gbAIIntermediate.Name = "_gbAIIntermediate";
            this._gbAIIntermediate.Size = new System.Drawing.Size(88, 312);
            this._gbAIIntermediate.TabIndex = 11;
            this._gbAIIntermediate.TabStop = false;
            this._gbAIIntermediate.Text = "Intermediate";
            // 
            // _tbAIIntermediateBeaconSearchDepth
            // 
            this._tbAIIntermediateBeaconSearchDepth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIIntermediateBeaconSearchDepth.Location = new System.Drawing.Point(6, 283);
            this._tbAIIntermediateBeaconSearchDepth.Name = "_tbAIIntermediateBeaconSearchDepth";
            this._tbAIIntermediateBeaconSearchDepth.Size = new System.Drawing.Size(76, 23);
            this._tbAIIntermediateBeaconSearchDepth.TabIndex = 9;
            // 
            // _tbAIIntermediateMaxBeaconHeightDifference
            // 
            this._tbAIIntermediateMaxBeaconHeightDifference.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIIntermediateMaxBeaconHeightDifference.Location = new System.Drawing.Point(6, 254);
            this._tbAIIntermediateMaxBeaconHeightDifference.Name = "_tbAIIntermediateMaxBeaconHeightDifference";
            this._tbAIIntermediateMaxBeaconHeightDifference.Size = new System.Drawing.Size(76, 23);
            this._tbAIIntermediateMaxBeaconHeightDifference.TabIndex = 8;
            // 
            // _tbAIIntermediateBackupTurnRate
            // 
            this._tbAIIntermediateBackupTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIIntermediateBackupTurnRate.Location = new System.Drawing.Point(6, 22);
            this._tbAIIntermediateBackupTurnRate.Name = "_tbAIIntermediateBackupTurnRate";
            this._tbAIIntermediateBackupTurnRate.Size = new System.Drawing.Size(76, 23);
            this._tbAIIntermediateBackupTurnRate.TabIndex = 0;
            // 
            // _tbAIIntermediateMaxBeaconDistance
            // 
            this._tbAIIntermediateMaxBeaconDistance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIIntermediateMaxBeaconDistance.Location = new System.Drawing.Point(6, 225);
            this._tbAIIntermediateMaxBeaconDistance.Name = "_tbAIIntermediateMaxBeaconDistance";
            this._tbAIIntermediateMaxBeaconDistance.Size = new System.Drawing.Size(76, 23);
            this._tbAIIntermediateMaxBeaconDistance.TabIndex = 7;
            // 
            // _tbAIIntermediateBackupGameCycles
            // 
            this._tbAIIntermediateBackupGameCycles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIIntermediateBackupGameCycles.Location = new System.Drawing.Point(6, 51);
            this._tbAIIntermediateBackupGameCycles.Name = "_tbAIIntermediateBackupGameCycles";
            this._tbAIIntermediateBackupGameCycles.Size = new System.Drawing.Size(76, 23);
            this._tbAIIntermediateBackupGameCycles.TabIndex = 1;
            // 
            // _tbAIIntermediateMaxDistToAcquireFlag
            // 
            this._tbAIIntermediateMaxDistToAcquireFlag.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIIntermediateMaxDistToAcquireFlag.Location = new System.Drawing.Point(6, 196);
            this._tbAIIntermediateMaxDistToAcquireFlag.Name = "_tbAIIntermediateMaxDistToAcquireFlag";
            this._tbAIIntermediateMaxDistToAcquireFlag.Size = new System.Drawing.Size(76, 23);
            this._tbAIIntermediateMaxDistToAcquireFlag.TabIndex = 6;
            // 
            // _tbAIIntermediateBackupSpeedFactor
            // 
            this._tbAIIntermediateBackupSpeedFactor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIIntermediateBackupSpeedFactor.Location = new System.Drawing.Point(6, 80);
            this._tbAIIntermediateBackupSpeedFactor.Name = "_tbAIIntermediateBackupSpeedFactor";
            this._tbAIIntermediateBackupSpeedFactor.Size = new System.Drawing.Size(76, 23);
            this._tbAIIntermediateBackupSpeedFactor.TabIndex = 2;
            // 
            // _tbAIIntermediateMaxDistToAcquireHuman
            // 
            this._tbAIIntermediateMaxDistToAcquireHuman.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIIntermediateMaxDistToAcquireHuman.Location = new System.Drawing.Point(6, 167);
            this._tbAIIntermediateMaxDistToAcquireHuman.Name = "_tbAIIntermediateMaxDistToAcquireHuman";
            this._tbAIIntermediateMaxDistToAcquireHuman.Size = new System.Drawing.Size(76, 23);
            this._tbAIIntermediateMaxDistToAcquireHuman.TabIndex = 5;
            // 
            // _tbAIIntermediateMaxTargetAngleAdjust
            // 
            this._tbAIIntermediateMaxTargetAngleAdjust.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIIntermediateMaxTargetAngleAdjust.Location = new System.Drawing.Point(6, 109);
            this._tbAIIntermediateMaxTargetAngleAdjust.Name = "_tbAIIntermediateMaxTargetAngleAdjust";
            this._tbAIIntermediateMaxTargetAngleAdjust.Size = new System.Drawing.Size(76, 23);
            this._tbAIIntermediateMaxTargetAngleAdjust.TabIndex = 3;
            // 
            // _tbAIIntermediateMaxAngleForAcceleration
            // 
            this._tbAIIntermediateMaxAngleForAcceleration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIIntermediateMaxAngleForAcceleration.Location = new System.Drawing.Point(6, 138);
            this._tbAIIntermediateMaxAngleForAcceleration.Name = "_tbAIIntermediateMaxAngleForAcceleration";
            this._tbAIIntermediateMaxAngleForAcceleration.Size = new System.Drawing.Size(76, 23);
            this._tbAIIntermediateMaxAngleForAcceleration.TabIndex = 4;
            // 
            // _gbAIBeginner
            // 
            this._gbAIBeginner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._gbAIBeginner.Controls.Add(this._tbAIBeginnerBeaconSearchDepth);
            this._gbAIBeginner.Controls.Add(this._tbAIBeginnerMaxBeaconHeightDifference);
            this._gbAIBeginner.Controls.Add(this._tbAIBeginnerMaxBeaconDistance);
            this._gbAIBeginner.Controls.Add(this._tbAIBeginnerMaxDistToAcquireFlag);
            this._gbAIBeginner.Controls.Add(this._tbAIBeginnerMaxDistToAcquireHuman);
            this._gbAIBeginner.Controls.Add(this._tbAIBeginnerMaxAngleForAcceleration);
            this._gbAIBeginner.Controls.Add(this._tbAIBeginnerMaxTargetAngleAdjust);
            this._gbAIBeginner.Controls.Add(this._tbAIBeginnerBackupSpeedFactor);
            this._gbAIBeginner.Controls.Add(this._tbAIBeginnerBackupGameCycles);
            this._gbAIBeginner.Controls.Add(this._tbAIBeginnerBackupTurnRate);
            this._gbAIBeginner.Location = new System.Drawing.Point(179, 6);
            this._gbAIBeginner.Name = "_gbAIBeginner";
            this._gbAIBeginner.Size = new System.Drawing.Size(88, 312);
            this._gbAIBeginner.TabIndex = 10;
            this._gbAIBeginner.TabStop = false;
            this._gbAIBeginner.Text = "Beginner";
            // 
            // _tbAIBeginnerBeaconSearchDepth
            // 
            this._tbAIBeginnerBeaconSearchDepth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIBeginnerBeaconSearchDepth.Location = new System.Drawing.Point(6, 283);
            this._tbAIBeginnerBeaconSearchDepth.Name = "_tbAIBeginnerBeaconSearchDepth";
            this._tbAIBeginnerBeaconSearchDepth.Size = new System.Drawing.Size(76, 23);
            this._tbAIBeginnerBeaconSearchDepth.TabIndex = 9;
            // 
            // _tbAIBeginnerMaxBeaconHeightDifference
            // 
            this._tbAIBeginnerMaxBeaconHeightDifference.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIBeginnerMaxBeaconHeightDifference.Location = new System.Drawing.Point(6, 254);
            this._tbAIBeginnerMaxBeaconHeightDifference.Name = "_tbAIBeginnerMaxBeaconHeightDifference";
            this._tbAIBeginnerMaxBeaconHeightDifference.Size = new System.Drawing.Size(76, 23);
            this._tbAIBeginnerMaxBeaconHeightDifference.TabIndex = 8;
            // 
            // _tbAIBeginnerMaxBeaconDistance
            // 
            this._tbAIBeginnerMaxBeaconDistance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIBeginnerMaxBeaconDistance.Location = new System.Drawing.Point(6, 225);
            this._tbAIBeginnerMaxBeaconDistance.Name = "_tbAIBeginnerMaxBeaconDistance";
            this._tbAIBeginnerMaxBeaconDistance.Size = new System.Drawing.Size(76, 23);
            this._tbAIBeginnerMaxBeaconDistance.TabIndex = 7;
            // 
            // _tbAIBeginnerMaxDistToAcquireFlag
            // 
            this._tbAIBeginnerMaxDistToAcquireFlag.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIBeginnerMaxDistToAcquireFlag.Location = new System.Drawing.Point(6, 196);
            this._tbAIBeginnerMaxDistToAcquireFlag.Name = "_tbAIBeginnerMaxDistToAcquireFlag";
            this._tbAIBeginnerMaxDistToAcquireFlag.Size = new System.Drawing.Size(76, 23);
            this._tbAIBeginnerMaxDistToAcquireFlag.TabIndex = 6;
            // 
            // _tbAIBeginnerMaxDistToAcquireHuman
            // 
            this._tbAIBeginnerMaxDistToAcquireHuman.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIBeginnerMaxDistToAcquireHuman.Location = new System.Drawing.Point(6, 167);
            this._tbAIBeginnerMaxDistToAcquireHuman.Name = "_tbAIBeginnerMaxDistToAcquireHuman";
            this._tbAIBeginnerMaxDistToAcquireHuman.Size = new System.Drawing.Size(76, 23);
            this._tbAIBeginnerMaxDistToAcquireHuman.TabIndex = 5;
            // 
            // _tbAIBeginnerMaxAngleForAcceleration
            // 
            this._tbAIBeginnerMaxAngleForAcceleration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIBeginnerMaxAngleForAcceleration.Location = new System.Drawing.Point(6, 138);
            this._tbAIBeginnerMaxAngleForAcceleration.Name = "_tbAIBeginnerMaxAngleForAcceleration";
            this._tbAIBeginnerMaxAngleForAcceleration.Size = new System.Drawing.Size(76, 23);
            this._tbAIBeginnerMaxAngleForAcceleration.TabIndex = 4;
            // 
            // _tbAIBeginnerMaxTargetAngleAdjust
            // 
            this._tbAIBeginnerMaxTargetAngleAdjust.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIBeginnerMaxTargetAngleAdjust.Location = new System.Drawing.Point(6, 109);
            this._tbAIBeginnerMaxTargetAngleAdjust.Name = "_tbAIBeginnerMaxTargetAngleAdjust";
            this._tbAIBeginnerMaxTargetAngleAdjust.Size = new System.Drawing.Size(76, 23);
            this._tbAIBeginnerMaxTargetAngleAdjust.TabIndex = 3;
            // 
            // _tbAIBeginnerBackupSpeedFactor
            // 
            this._tbAIBeginnerBackupSpeedFactor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIBeginnerBackupSpeedFactor.Location = new System.Drawing.Point(6, 80);
            this._tbAIBeginnerBackupSpeedFactor.Name = "_tbAIBeginnerBackupSpeedFactor";
            this._tbAIBeginnerBackupSpeedFactor.Size = new System.Drawing.Size(76, 23);
            this._tbAIBeginnerBackupSpeedFactor.TabIndex = 2;
            // 
            // _tbAIBeginnerBackupGameCycles
            // 
            this._tbAIBeginnerBackupGameCycles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIBeginnerBackupGameCycles.Location = new System.Drawing.Point(6, 51);
            this._tbAIBeginnerBackupGameCycles.Name = "_tbAIBeginnerBackupGameCycles";
            this._tbAIBeginnerBackupGameCycles.Size = new System.Drawing.Size(76, 23);
            this._tbAIBeginnerBackupGameCycles.TabIndex = 1;
            // 
            // _tbAIBeginnerBackupTurnRate
            // 
            this._tbAIBeginnerBackupTurnRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbAIBeginnerBackupTurnRate.Location = new System.Drawing.Point(6, 22);
            this._tbAIBeginnerBackupTurnRate.Name = "_tbAIBeginnerBackupTurnRate";
            this._tbAIBeginnerBackupTurnRate.Size = new System.Drawing.Size(76, 23);
            this._tbAIBeginnerBackupTurnRate.TabIndex = 0;
            // 
            // _btSaveRegistry
            // 
            this._btSaveRegistry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btSaveRegistry.Location = new System.Drawing.Point(487, 12);
            this._btSaveRegistry.Name = "_btSaveRegistry";
            this._btSaveRegistry.Size = new System.Drawing.Size(100, 23);
            this._btSaveRegistry.TabIndex = 1;
            this._btSaveRegistry.Text = "Save Registry";
            this._btSaveRegistry.UseVisualStyleBackColor = true;
            this._btSaveRegistry.Click += new System.EventHandler(this._btSaveRegistry_Click);
            // 
            // _btClearRegistry
            // 
            this._btClearRegistry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btClearRegistry.Location = new System.Drawing.Point(487, 70);
            this._btClearRegistry.Name = "_btClearRegistry";
            this._btClearRegistry.Size = new System.Drawing.Size(100, 23);
            this._btClearRegistry.TabIndex = 3;
            this._btClearRegistry.Text = "Clear Registry";
            this._btClearRegistry.UseVisualStyleBackColor = true;
            this._btClearRegistry.Click += new System.EventHandler(this._btClearRegistry_Click);
            // 
            // _btReloadRegistry
            // 
            this._btReloadRegistry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btReloadRegistry.Location = new System.Drawing.Point(487, 41);
            this._btReloadRegistry.Name = "_btReloadRegistry";
            this._btReloadRegistry.Size = new System.Drawing.Size(100, 23);
            this._btReloadRegistry.TabIndex = 2;
            this._btReloadRegistry.Text = "Reload Registry";
            this._btReloadRegistry.UseVisualStyleBackColor = true;
            this._btReloadRegistry.Click += new System.EventHandler(this._btReloadRegistry_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 433);
            this.Controls.Add(this._btReloadRegistry);
            this.Controls.Add(this._btClearRegistry);
            this.Controls.Add(this._btSaveRegistry);
            this.Controls.Add(this._tcSettings);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HoverHack";
            this._tcSettings.ResumeLayout(false);
            this._tpPlayer.ResumeLayout(false);
            this._tpPlayer.PerformLayout();
            this._gbPlayerSpeedMultiplier.ResumeLayout(false);
            this._gbPlayerSpeedMultiplier.PerformLayout();
            this._gbPlayerExpert.ResumeLayout(false);
            this._gbPlayerExpert.PerformLayout();
            this._gbPlayerIntermediate.ResumeLayout(false);
            this._gbPlayerIntermediate.PerformLayout();
            this._gbPlayerBeginner.ResumeLayout(false);
            this._gbPlayerBeginner.PerformLayout();
            this._tpDrone.ResumeLayout(false);
            this._tpDrone.PerformLayout();
            this._gbDroneSpeedMultiplier.ResumeLayout(false);
            this._gbDroneSpeedMultiplier.PerformLayout();
            this._gbDroneExpert.ResumeLayout(false);
            this._gbDroneExpert.PerformLayout();
            this._gbDroneIntermediate.ResumeLayout(false);
            this._gbDroneIntermediate.PerformLayout();
            this._gbDroneBeginner.ResumeLayout(false);
            this._gbDroneBeginner.PerformLayout();
            this._tpAI.ResumeLayout(false);
            this._tpAI.PerformLayout();
            this._gbAIExpert.ResumeLayout(false);
            this._gbAIExpert.PerformLayout();
            this._gbAIIntermediate.ResumeLayout(false);
            this._gbAIIntermediate.PerformLayout();
            this._gbAIBeginner.ResumeLayout(false);
            this._gbAIBeginner.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _tcSettings;
        private System.Windows.Forms.TabPage _tpPlayer;
        private System.Windows.Forms.TabPage _tpDrone;
        private System.Windows.Forms.TabPage _tpAI;
        private System.Windows.Forms.Button _btSaveRegistry;
        private System.Windows.Forms.Button _btClearRegistry;
        private System.Windows.Forms.Label _lbPlayerMaxVelocity;
        private System.Windows.Forms.GroupBox _gbPlayerBeginner;
        private FloatTextBox _tbPlayerBeginnerMaxVelocity;
        private System.Windows.Forms.GroupBox _gbPlayerSpeedMultiplier;
        private FloatTextBox _tbPlayerExpertSlowMultiplier;
        private FloatTextBox _tbPlayerIntermediateSlowMultiplier;
        private FloatTextBox _tbPlayerBeginnerSlowMultiplier;
        private FloatTextBox _tbPlayerExpertSpeedMultiplier;
        private FloatTextBox _tbPlayerIntermediateSpeedMultiplier;
        private FloatTextBox _tbPlayerBeginnerSpeedMultiplier;
        private System.Windows.Forms.Label _lbPlayerSlowMultiplier;
        private System.Windows.Forms.Label _lbPlayerSpeedMultiplier;
        private System.Windows.Forms.Label _lbPlayerRotationAccel;
        private System.Windows.Forms.Label _lbPlayerRotationKick;
        private System.Windows.Forms.Label _lbPlayerRotationStability;
        private System.Windows.Forms.Label _lbPlayerTurnLagFactor;
        private System.Windows.Forms.Label _lbPlayerFrictionConstant;
        private System.Windows.Forms.Label _lbPlayerTurnRate;
        private System.Windows.Forms.Label _lbPlayerNormalAccel;
        private System.Windows.Forms.GroupBox _gbPlayerExpert;
        private FloatTextBox _tbPlayerExpertRotationAccel;
        private FloatTextBox _tbPlayerExpertRotationKick;
        private FloatTextBox _tbPlayerExpertRotationStability;
        private FloatTextBox _tbPlayerExpertTurnLagFactor;
        private FloatTextBox _tbPlayerExpertFrictionConstant;
        private FloatTextBox _tbPlayerExpertTurnRate;
        private FloatTextBox _tbPlayerExpertNormalAccel;
        private FloatTextBox _tbPlayerExpertMaxVelocity;
        private System.Windows.Forms.GroupBox _gbPlayerIntermediate;
        private FloatTextBox _tbPlayerIntermediateRotationAccel;
        private FloatTextBox _tbPlayerIntermediateRotationKick;
        private FloatTextBox _tbPlayerIntermediateRotationStability;
        private FloatTextBox _tbPlayerIntermediateTurnLagFactor;
        private FloatTextBox _tbPlayerIntermediateFrictionConstant;
        private FloatTextBox _tbPlayerIntermediateTurnRate;
        private FloatTextBox _tbPlayerIntermediateNormalAccel;
        private FloatTextBox _tbPlayerIntermediateMaxVelocity;
        private FloatTextBox _tbPlayerBeginnerRotationAccel;
        private FloatTextBox _tbPlayerBeginnerRotationKick;
        private FloatTextBox _tbPlayerBeginnerRotationStability;
        private FloatTextBox _tbPlayerBeginnerTurnLagFactor;
        private FloatTextBox _tbPlayerBeginnerFrictionConstant;
        private FloatTextBox _tbPlayerBeginnerTurnRate;
        private FloatTextBox _tbPlayerBeginnerNormalAccel;
        private System.Windows.Forms.Label _lbDroneRotationAccel;
        private System.Windows.Forms.Label _lbDroneRotationKick;
        private System.Windows.Forms.Label _lbDroneRotationStability;
        private System.Windows.Forms.Label _lbDroneTurnLagFactor;
        private System.Windows.Forms.Label _lbDroneFrictionConstant;
        private System.Windows.Forms.Label _lbDroneTurnRate;
        private System.Windows.Forms.Label _lbDroneNormalAccel;
        private System.Windows.Forms.Label _lbDroneAcquireVelocity;
        private System.Windows.Forms.Label _lbDroneMaxVelocity;
        private System.Windows.Forms.GroupBox _gbDroneExpert;
        private FloatTextBox _tbDroneExpertRotationAccel;
        private FloatTextBox _tbDroneExpertMaxVelocity;
        private FloatTextBox _tbDroneExpertFrictionConstant;
        private FloatTextBox _tbDroneExpertRotationKick;
        private FloatTextBox _tbDroneExpertTurnRate;
        private FloatTextBox _tbDroneExpertAquireVelocity;
        private FloatTextBox _tbDroneExpertTurnLagFactor;
        private FloatTextBox _tbDroneExpertRotationStability;
        private FloatTextBox _tbDroneExpertNormalAccel;
        private System.Windows.Forms.GroupBox _gbDroneIntermediate;
        private FloatTextBox _tbDroneIntermediateRotationAccel;
        private FloatTextBox _tbDroneIntermediateMaxVelocity;
        private FloatTextBox _tbDroneIntermediateRotationKick;
        private FloatTextBox _tbDroneIntermediateAquireVelocity;
        private FloatTextBox _tbDroneIntermediateRotationStability;
        private FloatTextBox _tbDroneIntermediateNormalAccel;
        private FloatTextBox _tbDroneIntermediateTurnLagFactor;
        private FloatTextBox _tbDroneIntermediateTurnRate;
        private FloatTextBox _tbDroneIntermediateFrictionConstant;
        private System.Windows.Forms.GroupBox _gbDroneBeginner;
        private FloatTextBox _tbDroneBeginnerRotationAccel;
        private FloatTextBox _tbDroneBeginnerRotationKick;
        private FloatTextBox _tbDroneBeginnerRotationStability;
        private FloatTextBox _tbDroneBeginnerTurnLagFactor;
        private FloatTextBox _tbDroneBeginnerFrictionConstant;
        private FloatTextBox _tbDroneBeginnerTurnRate;
        private FloatTextBox _tbDroneBeginnerNormalAccel;
        private FloatTextBox _tbDroneBeginnerAquireVelocity;
        private FloatTextBox _tbDroneBeginnerMaxVelocity;
        private System.Windows.Forms.Label _lbAIBeaconSearchDepth;
        private System.Windows.Forms.Label _lbAIMaxBeaconHeightDifference;
        private System.Windows.Forms.Label _lbAIMaxBeaconDistance;
        private System.Windows.Forms.Label _lbAIMaxDistanceToAcquireFlag;
        private System.Windows.Forms.Label _lbAIMaxDistToAcquireHuman;
        private System.Windows.Forms.Label _lbAIMaxAngleForAcceleration;
        private System.Windows.Forms.Label _lbAIMaxTargetAngleAdjust;
        private System.Windows.Forms.Label _lbAIBackupSpeedFactor;
        private System.Windows.Forms.Label _lbAIBackupGameCycles;
        private System.Windows.Forms.Label _lbAIBackupTurnRate;
        private System.Windows.Forms.GroupBox _gbAIExpert;
        private IntegerTextBox _tbAIExpertBeaconSearchDepth;
        private IntegerTextBox _tbAIExpertMaxBeaconHeightDifference;
        private FloatTextBox _tbAIExpertBackupTurnRate;
        private IntegerTextBox _tbAIExpertMaxAngleForAcceleration;
        private IntegerTextBox _tbAIExpertMaxBeaconDistance;
        private IntegerTextBox _tbAIExpertMaxTargetAngleAdjust;
        private IntegerTextBox _tbAIExpertBackupGameCycles;
        private IntegerTextBox _tbAIExpertMaxDistToAcquireHuman;
        private IntegerTextBox _tbAIExpertMaxDistToAcquireFlag;
        private FloatTextBox _tbAIExpertBackupSpeedFactor;
        private System.Windows.Forms.GroupBox _gbAIIntermediate;
        private IntegerTextBox _tbAIIntermediateBeaconSearchDepth;
        private IntegerTextBox _tbAIIntermediateMaxBeaconHeightDifference;
        private FloatTextBox _tbAIIntermediateBackupTurnRate;
        private IntegerTextBox _tbAIIntermediateMaxBeaconDistance;
        private IntegerTextBox _tbAIIntermediateBackupGameCycles;
        private IntegerTextBox _tbAIIntermediateMaxDistToAcquireFlag;
        private FloatTextBox _tbAIIntermediateBackupSpeedFactor;
        private IntegerTextBox _tbAIIntermediateMaxDistToAcquireHuman;
        private IntegerTextBox _tbAIIntermediateMaxTargetAngleAdjust;
        private IntegerTextBox _tbAIIntermediateMaxAngleForAcceleration;
        private System.Windows.Forms.GroupBox _gbAIBeginner;
        private IntegerTextBox _tbAIBeginnerBeaconSearchDepth;
        private IntegerTextBox _tbAIBeginnerMaxBeaconHeightDifference;
        private IntegerTextBox _tbAIBeginnerMaxBeaconDistance;
        private IntegerTextBox _tbAIBeginnerMaxDistToAcquireFlag;
        private IntegerTextBox _tbAIBeginnerMaxDistToAcquireHuman;
        private IntegerTextBox _tbAIBeginnerMaxAngleForAcceleration;
        private IntegerTextBox _tbAIBeginnerMaxTargetAngleAdjust;
        private FloatTextBox _tbAIBeginnerBackupSpeedFactor;
        private IntegerTextBox _tbAIBeginnerBackupGameCycles;
        private FloatTextBox _tbAIBeginnerBackupTurnRate;
        private FloatTextBox _tbPlayerExpertAcquireVelocity;
        private FloatTextBox _tbPlayerIntermediateAcquireVelocity;
        private FloatTextBox _tbPlayerBeginnerAcquireVelocity;
        private System.Windows.Forms.Label _lbPlayerAcquireVelocity;
        private FloatTextBox _tbDroneExpertSlowMultiplier;
        private FloatTextBox _tbDroneIntermediateSlowMultiplier;
        private FloatTextBox _tbDroneBeginnerSlowMultiplier;
        private System.Windows.Forms.Label _lbDroneSlowMultiplier;
        private FloatTextBox _tbDroneExpertSpeedMultiplier;
        private FloatTextBox _tbDroneIntermediateSpeedMultiplier;
        private FloatTextBox _tbDroneBeginnerSpeedMultiplier;
        private System.Windows.Forms.Label _lbDroneSpeedMultiplier;
        private System.Windows.Forms.GroupBox _gbDroneSpeedMultiplier;
        private System.Windows.Forms.Button _btReloadRegistry;
    }
}

