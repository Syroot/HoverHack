﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Syroot.HoverHack
{
    /// <summary>
    /// Represents a <see cref="TextBox"/> only accepting specific numerical input.
    /// </summary>
    public class NumericTextBox : TextBox
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericTextBox"/> class.
        /// </summary>
        public NumericTextBox()
        {
            TextAlign = HorizontalAlignment.Right;
        }

        // ---- EVENTS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Occurs when the represented value has been changed.
        /// </summary>
        public event EventHandler ValueChanged;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the background color of the control.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }

        /// <summary>
        /// Gets or sets how text is aligned in a <see cref="FloatTextBox"/> control.
        /// </summary>
        [DefaultValue(HorizontalAlignment.Right)]
        public new HorizontalAlignment TextAlign
        {
            get
            {
                return base.TextAlign;
            }
            set
            {
                base.TextAlign = value;
            }
        }

        /// <summary>
        /// Gets or sets the current text in the <see cref="NumericTextBox"/>.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = TrySetValue(value);
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Raises the <see cref="LostFocus"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnLostFocus(EventArgs e)
        {
            Text = Text;
            OnValueChanged(e);
        }
        
        /// <summary>
        /// Raises the <see cref="ValueChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnValueChanged(EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, e);
            }
        }

        /// <summary>
        /// Tries to return the numerical value of the text in the <see cref="NumericTextBox"/>.
        /// </summary>
        /// <returns>The numerical value of the text in the <see cref="NumericTextBox"/>.</returns>
        protected virtual object TryGetValue()
        {
            return null;
        }

        /// <summary>
        /// Tries to set the given text as the new numerical value of the <see cref="NumericTextBox"/>.
        /// </summary>
        /// <param name="text">The text to convert into the numerical representation.</param>
        /// <returns>The text to eventually display in the <see cref="NumericTextBox"/>.</returns>
        protected virtual string TrySetValue(string text)
        {
            return null;
        }
    }
}
