﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("HoverHack")]
[assembly: AssemblyDescription("Hover! Registry Manipulation Tool")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Syroot")]
[assembly: AssemblyProduct("Syroot.HoverHack")]
[assembly: AssemblyCopyright("(c) Syroot, licensed under MIT")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("1.2.0.0")]
[assembly: AssemblyFileVersion("1.2.0.0")]

[assembly: ComVisible(false)]
[assembly: Guid("7eb4c160-79d1-4fc4-887b-537b5212ff2a")]