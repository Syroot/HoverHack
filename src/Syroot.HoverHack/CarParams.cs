﻿using System.IO;

namespace Syroot.HoverHack
{
    /// <summary>
    /// Represents car configuration parameters.
    /// </summary>
    internal class CarParams
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="CarParams"/> class from the given byte array.
        /// </summary>
        /// <param name="rawData">The raw byte array data.</param>
        internal CarParams(byte[] rawData)
        {
            using (MemoryStream stream = new MemoryStream(rawData))
            using (BinaryReader reader = new BinaryReader(stream))
            {
                NormalAccel = reader.ReadInt32() / 65536f;
                FrictionConstant = reader.ReadInt32() / 65536f;
                MaxVelocity = reader.ReadInt32() / 65536f;
                AcquireVelocity = reader.ReadInt32() / 65536f;
                RotationAccel = reader.ReadInt32() / 65536f;
                RotationKick = reader.ReadInt32() / 65536f;
                RotationStability = reader.ReadInt32() / 65536f;
                SlowMultiplier = reader.ReadInt32() / 65536f;
                SpeedMultiplier = reader.ReadInt32() / 65536f;
                TurnLagFactor = reader.ReadInt32() / 65536f;
                TurnRate = reader.ReadInt32() / 65536f;
            }
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the amount of acceleration until reaching maximum velocity.
        /// </summary>
        internal float NormalAccel { get; set; }

        /// <summary>
        /// Gets or sets the amount of sliding when acceleration pauses.
        /// </summary>
        internal float FrictionConstant { get; set; }

        /// <summary>
        /// Gets or sets the maximum speed a car can reach.
        /// </summary>
        internal float MaxVelocity { get; set; }

        /// <summary>
        /// Gets or sets the speed an opponent car has when running towards a flag or a human. Unused for the human car.
        /// </summary>
        internal float AcquireVelocity { get; set; }

        /// <summary>
        /// Gets or sets the amount of spinning after hitting something.
        /// </summary>
        internal float RotationStability { get; set; }

        /// <summary>
        /// Gets or sets the amount of immediate rotation after hitting something.
        /// </summary>
        internal float RotationKick { get; set; }

        /// <summary>
        /// Gets or sets the speed of rotational skidding when turning manually.
        /// </summary>
        internal float RotationAccel { get; set; }

        /// <summary>
        /// Gets or sets the multiplication factor a car gets slower when under the influence of a red light. This has
        /// no effect on opponent cars as they can't collect pods.
        /// </summary>
        internal float SlowMultiplier { get; set; }

        /// <summary>
        /// Gets or sets the multiplication factor a car gets faster when under the influence of a green light. This is
        /// also the speed of the skid pads and sets the maximum speed displayed in the speedometer.
        /// </summary>
        internal float SpeedMultiplier { get; set; }

        /// <summary>
        /// Gets or sets the amount of skidding introduced when turning (e.g., the "hovering" effect amount).
        /// </summary>
        internal float TurnLagFactor { get; set; }

        /// <summary>
        /// Gets or sets the speed of rotation when turning manually.
        /// </summary>
        internal float TurnRate { get; set; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Serializes the parameters back into a raw byte array.
        /// </summary>
        /// <returns>The serialized byte array.</returns>
        internal byte[] GetRawData()
        {
            byte[] buffer = new byte[44];

            using (MemoryStream stream = new MemoryStream(buffer, true))
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write((int)(NormalAccel * 65536));
                writer.Write((int)(FrictionConstant * 65536));
                writer.Write((int)(MaxVelocity * 65536));
                writer.Write((int)(AcquireVelocity * 65536));
                writer.Write((int)(RotationAccel * 65536));
                writer.Write((int)(RotationKick * 65536));
                writer.Write((int)(RotationStability * 65536));
                writer.Write((int)(SlowMultiplier * 65536));
                writer.Write((int)(SpeedMultiplier * 65536));
                writer.Write((int)(TurnLagFactor * 65536));
                writer.Write((int)(TurnRate * 65536));
            }

            return buffer;
        }
    }
}
