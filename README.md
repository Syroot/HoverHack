# HoverHack
Game physics modification tool for the Windows 95 game "Hover!"

"Hover!" is a game which combines bumper cars with capture the flag. It has been released with Windows 95.

The beta versions of the game included several debugging material, one of which was the modification of bumper car physics and AI behavior parameters. The dialogs to edit these settings have been removed at an early stage, but the functionality remains and can be controlled by setting specific registry keys in a strict format.

This tool simulates the original dialogs and creates the registry keys automatically, by letting you simply enter float or integer numbers to change the behavior as you want it. After you restart the game, the changes show immediate effect.
